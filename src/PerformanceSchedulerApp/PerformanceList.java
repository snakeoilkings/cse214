package PerformanceSchedulerApp;
/**
 * PerformanceList implements the PerformanceNode class to create a linked
 * list of PerformanceNodes
 * 
 * Homework 2
 * CSE 214
 * Recitation #6
 * TA: Kevin Flanygolts
 * Grader: Zheyuan (Jeffrey) Gao
 * 
 * @author Kathryn Blecher
 * email: kathryn.blecher@gmail.com
 * Stony Brook ID: 108871623
 */
public class PerformanceList {
	
	private int listSize;		     //the number of nodes in the list
	private int cursorPos;		     //the current position of the cursor
	private PerformanceNode head;    //the first node in the list
	private PerformanceNode cursor;  //the node of focus
	private PerformanceNode tail;    //the last node in the list
	/**
	 * Constructor for the PerformanceList object
	 */
	public PerformanceList() {
		head = null;
		cursor = null;
		tail = null;
	}
	/**
	 * Adds a PerformanceNode to the end of the linked list, and sets the tail
	 * and the cursor as the newly added node. If no nodes exist, it sets it to
	 * the head, tail and cursor
	 * @param newPerformance
	 * 		the new node to be added to the linked list
	 * postconditions: startTimeInMinutes of the new node is updated so that the
	 * 		start time is either 0 if it's the only node, or the start time of 
	 * 		the previous node plus the previous node's lengthInMinutes.
	 * 		listSize is also incremented by 1, and currentPos is set to the new
	 * 		listSize.
	 */
	public void addToEnd(PerformanceNode newPerformance) {
		if (tail == null) {
			tail = newPerformance;
			head = tail;
			cursor = tail;
		}
		else {
			newPerformance.setPrev(tail);
			newPerformance.setTime();
			tail.setNext(newPerformance);
			PerformanceNode temp = new PerformanceNode();
			temp = tail;
			tail = newPerformance;
			cursor = tail;     
			cursor.setPrev(temp);
		}
		listSize++;
		cursorPos = listSize;
	}
	/**
	 * Adds a node after the cursor. If the cursor doesn't exist, addToEnd is 
	 * called for the node. If the cursor is currently the tail, it will update
	 * the tail as the new node
	 * @param newPerformance
	 * 		the new PerformanceNode object to be added to the linked list
	 * postconditions: cursor will now equal the new node, startInMinutes will
	 * 		be updated for the new node as the next nodes start time, and every
	 * 		node after by adding the new nodes length to each node after it.
	 * 		If the cursor was originally on the tail, the tail will be updated
	 * 		to the new node. listSize will be incremented by one.
	 */
	public void addAfterCurrent(PerformanceNode newPerformance) {
		if (cursor == null) 
			addToEnd(newPerformance);
		else {
			newPerformance.setPrev(cursor);
			newPerformance.setTime();
			newPerformance.setNext(cursor.getNext());
			cursor.setNext(newPerformance);
			cursor = cursor.getNext();
			cursor.setPrev(newPerformance.getPrev());
			PerformanceNode temp = cursor;		
			for(int i=cursorPos; i<listSize;i++) {		
				temp = temp.getNext();				
				temp.setStartInMinutes(temp.getStartInMinutes() + 
						newPerformance.getLengthInMinutes());
			}
			cursorPos++;
		}
		if (tail.getNext() != null) {
			tail = tail.getNext();
			tail.setPrev(newPerformance.getPrev());
			tail.setNext(null);
		}
		listSize++;
	}
	/**
	 * Deletes the current node. If the node is the head or tail it will set
	 * the appropriate new values for them (next for head, prev for tail). If
	 * there is only one item in the list, it sets head, tail, cursor = null
	 * postconditions: cursor will now equal the next node unless the tail was
	 * 		deleted, in which case the cursor will equal the previous node. 
	 * 		listSize will decrement by one, cursorPos will decrement by one if
	 * 		the tail was deleted. lengthInMinutes will change for every element
	 * 		following the deleted node. Their new values will all be decreased
	 * 	    by the length of time of the deleted node.
	 * @return 
	 * 		a boolean value, if the node was successfully removes it will
	 * 		return true, otherwise returns false
	 */
	public boolean removeCurrentNode() { 
		if (cursor != null) {
			if ((cursor.getNext() != null)) {
				if (cursor == head) 
					head = head.getNext();
				else 
					cursor.getPrev().setNext(cursor.getNext());
				PerformanceNode temp = cursor;
				int timeLost = temp.getLengthInMinutes();
				cursor = cursor.getNext();
				for (int i = cursorPos; i < listSize-1; i++) {
				temp = temp.getNext();
				temp.setStartInMinutes(temp.getStartInMinutes()- timeLost);
				}
			}
			else if (listSize == 1) {
				cursor = null;
				head = null;
				tail = null;
			}
			else {
				tail = cursor.getPrev();
				cursor = cursor.getPrev();
				cursor.setNext(null);
				tail.setNext(null);
				cursorPos--;
			}
			listSize--;
			return true;
		}
		return false;
	}
	/**
	 * Prints to console the current node of focus if it exists.
	 */
	public void displayCurrentPerformance() {
		if (cursor == null) {
			System.out.print("No element to display. There are no items in the "
					+ "list");
		}
		else {
			String output = new String("");
			output = String.format("%-8s%-6s%-25s%-25s%-17s%-15s%-4s","Current"
					,"No.", "Performance","Lead Performer Name", "Participants"
					, "Duration", "Start");
			output = output + "\n---------------------------------------------"
					+ "-------------------------------------------------------"
					+ "--\n*       " +cursorPos;
			System.out.print(output + cursor);
		}
	}
	/**
	 * Moves the cursor forward by one
	 * postconditions: cursor will now equal the node after it if possible.
	 * @return 
	 * 		a boolean value, if the node was successfully moved forward it will
	 * 		return true, otherwise returns false
	 * @throws IllegalArgumentException if the cursor is null.
	 */
	public boolean moveCursorForward() {
		if (cursor == null)
			throw new IllegalArgumentException("There is no cursor. List is "
			  + "empty."); 
		if (cursor.getNext() != null) {
			cursor = cursor.getNext();
			return true;
		}
		else
			return false;
			
	}
	/**
	 * Moves the cursor backwards by one
	 * postconditions: cursor will now equal the node before it if possible.
	 * @return 
	 * 		a boolean value, if the node was successfully moved backwards it 
	 * 		will return true, otherwise returns false
	 * @throws IllegalArgumentException if the cursor is currently null
	 */
	public boolean moveCursorBackward() {
		if (cursor == null)
			throw new IllegalArgumentException("There is no cursor. List is "
			  + " empty.");
		if (cursor.getPrev() != null) {
			cursor = cursor.getPrev();
			cursorPos--;
			return true;
		}
		else
			return false;
	}
	/**
	 * Moves the cursor to a user selected position.
	 * postconditions: cursor will now equal the node selected by user if
	 * 		possible.
	 * @return 
	 * 		a boolean value, if the node was successfully moved to position it 
	 * 		will return true, otherwise returns false
	 * @throws IllegalArgumentException if you try to move the cursor in to
	 * 		a null (non-existent) position.
	 */
	public boolean jumpToPosition(int position) {
		if ((position <= 0) || (position > listSize))
			return false;
		PerformanceNode temp = head;
		if (position <= listSize/2) 		   //This distinction shortens the
			for (int i=1; i < position; i++)   //search for the position. If 
				temp = temp.getNext();		   //the jump to is closer to the
		else {								   //head, it will start search for
			temp = tail;					   //the index there.
			for (int i=listSize; i > position; i--) 
				temp = temp.getPrev();
		}	
		cursor = temp;
		return true;
	}
	/**
	 * @return a neatly formatted String showing all performance information.
	 */
	public String toString() {
		String output = new String("");
		PerformanceNode temp = head;
		output = String.format("%-8s%-6s%-25s%-25s%-17s%-15s%-4s","Current","No.", "Performance",
				"Lead Performer Name", "Participants", "Duration", "Start");
		output = output + "\n-------------------------------------------------"
				+ "-----------------------------------------------------\n";
		if (cursor == head)
			output = output + "*       01." + head.toString() + "\n";
		else
			output = output + "        01." + head.toString() + "\n";
		for (int i=1; i < listSize; i++) {
			output = output + ((cursor == temp.getNext()) ? "*" : " ") + "    " + 
			  "   " + ((i<9) ? "0" : "") + (i+1) + "." + temp.getNext() + "\n";
			if (temp.getNext() != null)
				temp = temp.getNext();
		}
		return output;
	}
	
	public void showHead() {
		System.out.print(head);
	}
	public void showTail() {
		System.out.print(tail);
	}
}
