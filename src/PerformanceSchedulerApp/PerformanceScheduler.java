package PerformanceSchedulerApp;

/**
 * PerformanceScheduler implements PerformanceList class to create an editable
 * list of performers to create a final performance list with set times.
 * 
 * Homework 2
 * CSE 214
 * Recitation #6
 * TA: Kevin Flanygolts
 * Grader: Zheyuan (Jeffrey) Gao
 * 
 * @author Kathryn Blecher
 * email: kathryn.blecher@gmail.com
 * Stony Brook ID: 108871623
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class PerformanceScheduler {

	public static void main(String[] args) throws IOException {
		InputStreamReader inStream = new InputStreamReader(System.in);
		BufferedReader stdin = new BufferedReader(inStream);
		PerformanceList a = new PerformanceList();		
		PerformanceNode aa = new PerformanceNode("a","a",3,30);
		PerformanceNode bb = new PerformanceNode("b","b",3,30);
		PerformanceNode cc = new PerformanceNode("c","c",3,30);
		PerformanceNode dd = new PerformanceNode("d","d",3,30);
		PerformanceNode ee = new PerformanceNode("e","e",3,30);
		PerformanceNode ff = new PerformanceNode("f","f",3,30);
		PerformanceNode gg = new PerformanceNode("g","g",3,30);
		PerformanceNode hh = new PerformanceNode("h","h",3,30);
		PerformanceNode ii = new PerformanceNode("i","i",3,30);
		PerformanceNode jj = new PerformanceNode("j","j",3,30);
		PerformanceNode kk = new PerformanceNode("k","k",3,100);
		PerformanceNode ll = new PerformanceNode("l","l",3,30);
		PerformanceNode mm = new PerformanceNode("m","m",3,30);
		a.addToEnd(aa);a.addToEnd(bb);a.addToEnd(cc);a.addToEnd(dd);
		a.addToEnd(ee);a.addToEnd(ff);a.addToEnd(gg);a.addToEnd(hh);
		a.addToEnd(ii);a.addToEnd(jj);a.addToEnd(kk);a.addToEnd(ll);
		a.addToEnd(mm);
		
		
		char choice = ' ';
		while (choice != Character.toLowerCase('q')) {
			System.out.println("Main menu:" +"\n"+ "A) Add Performance\n"
			  + "I) Insert Performance\n"+ "R) Remove Current Performance\n"
			  + "D) Display All Performances\n" + "C) Display Current "
			  + "Performance\n" + "F) Move Cursor Forward\n" + "B) Move Cursor"
			  + " Backwards\n" + "J) Jump to Position\n"+ "Q) Quit\n");
			System.out.print("Select an option: ");
			choice = (char) System.in.read();
			String syntax = stdin.readLine();
			switch(choice)
				{
			case 'a': ;
			case 'A': menuAdd(a);
				      break;
			case 'i': ;
			case 'I': menuInsert(a);
				      break;
			case 'r': ;
			case 'R': menuRemove(a);
				      break;
			case 'd': ;
			case 'D': menuDisplayAll(a);
					  break;
			case 'c': ;
			case 'C': menuDisplayCurrent(a);
				  	  break; 
			case 'f': ;
			case 'F': menuForward(a);
					  break;
			case 'b': ;
			case 'B': menuBackward(a);
				  	  break;
			case 'j': ;
			case 'J': menuJump(a);
				  	  break;
			case 't': ;
			case 'T': a.showTail();
				  	  break;
			case 'h': ;
			case 'H': a.showHead();
				  	  break;
			case 'q': ;
			case 'Q': System.out.print("\nProgram Exited!");
					  break;
			}
	}
}
	/**
	 * Calls the jumpToPosition method and alerts the user if the move was 
	 * successful or not.
	 * @param a 
	 * 		the linked list of performance nodes
	 * postconditions: the cursor will now equal the new position if possible
	 * 		and moveCursor will equal the boolean value of the move's success.
	 * @throws IOException 
	 * 		if input is invalid.
	 */
	private static void menuJump(PerformanceList a) throws IOException {
		InputStreamReader inStream = new InputStreamReader(System.in);
		BufferedReader stdin = new BufferedReader(inStream);
		System.out.print("\nEnter the position number to become current: ");
		int pos = Integer.parseInt(stdin.readLine());
		boolean moveCursor = a.jumpToPosition(pos);
		if(moveCursor)
			System.out.print("Success! Current Node Moved to " + pos + "!");
		else
			System.out.print("Current Node Move Failed.");
		String wait = stdin.readLine();
	}
	/**
	 * Calls the moveCursorBackward method and alerts the user if the move 
	 * was successful or not.
	 * @param a 
	 * 		the performance node linked list
	 * postconditions: cursor will now equal the node before the current node, 
	 * 		and moveCursor will equal the boolean value of the ability of the
	 * 		node to be moved. Cursor will not be changed it the move is not
	 * 		possible.
	 * @throws IOException 
	 * 		if input is invalid
	 */
	private static void menuBackward(PerformanceList a) throws IOException {
		InputStreamReader inStream = new InputStreamReader(System.in);
		BufferedReader stdin = new BufferedReader(inStream);
		boolean moveCursor = a.moveCursorBackward();
		if(moveCursor)
			System.out.print("Success! Current Node Moved Back One!");
		else
			System.out.print("Current Node Move Failed.");
		String wait = stdin.readLine();
	}
	/**
	 * Calls the moveCursorForward method and alerts the user if the move was
	 * successful
	 * @param a 
	 * 		the performance node list
	 * postconditions: cursor will now equal the node after the current node, 
	 * 		and moveCursor will equal the boolean value of the ability of the
	 * 		node to be moved. Cursor will not change if the move is not
	 * 		possible.
	 * @throws IOException
	 * 		if the input is invalid.
	 */
	private static void menuForward(PerformanceList a) throws IOException {
		InputStreamReader inStream = new InputStreamReader(System.in);
		BufferedReader stdin = new BufferedReader(inStream);
		boolean moveCursor = a.moveCursorForward();
		if(moveCursor)
			System.out.print("Success! Current Node Moved Forward One!");
		else
			System.out.print("Current Node Move Failed.");
		String wait = stdin.readLine();
	}
	/**
	 * Displays the cursor by calling displayCurrentPerformance method.
	 * @param a the linked list of performance nodes
	 * @throws IOException if input in invalid
	 */
	private static void menuDisplayCurrent(PerformanceList a) throws IOException {
		InputStreamReader inStream = new InputStreamReader(System.in);
		BufferedReader stdin = new BufferedReader(inStream);
		System.out.print("Current Node:\n");
		a.displayCurrentPerformance();
		String wait = stdin.readLine();
	}
	/**
	 * Prints out the entire list of performance nodes using the toString 
	 * method of the PerformanceList class
	 * @param a list of performance nodes
	 * @throws IOException if input invalid
	 */
	private static void menuDisplayAll(PerformanceList a) throws IOException {
		InputStreamReader inStream = new InputStreamReader(System.in);
		BufferedReader stdin = new BufferedReader(inStream);
		System.out.print(a);	
		String wait = stdin.readLine();
	}
	/**
	 * Removes the item of focus by calling the removeCurrentNode method.
	 * @param a 
	 * 		the linked list of performance nodes
	 * postconditions: the cursor will now equal the node after the removed
	 * 		node.
	 * @throws IOException
	 * 		if input is invalid.
	 */
	private static void menuRemove(PerformanceList a) throws IOException {
		InputStreamReader inStream = new InputStreamReader(System.in);
		BufferedReader stdin = new BufferedReader(inStream);
		boolean didRemove = a.removeCurrentNode();
		if(didRemove)
			System.out.print("Success! Current Node Removed!");
		else
			System.out.print("Current Node Removal Failed.");
		String wait = stdin.readLine();
	}
	/**
	 * Inserts a new node after the current position and sets the cursor as the
	 * new node by calling the addAfterCurrent method. Then prints out the list
	 * @param a
	 * 		the linked list of performance nodes
	 * postconditions: the cursor will now be set to the newly inserted node.
	 * @throws IOException
	 * 		if input is invalid.
	 */
	private static void menuInsert(PerformanceList a) throws IOException {
		InputStreamReader inStream = new InputStreamReader(System.in);
		BufferedReader stdin = new BufferedReader(inStream);
		
		PerformanceNode newNode = new PerformanceNode();
		String name = new String("");
		String lead = new String("");
		int minutes = 0;
		int numParticipants = 0;
		System.out.print("Enter performance name: ");
		name = stdin.readLine();
		System.out.print("Enter performance lead: ");
		lead = stdin.readLine();
		System.out.print("Enter number of performers: ");
		numParticipants = Integer.parseInt(stdin.readLine());
		System.out.print("Enter performance length: ");
		minutes = Integer.parseInt(stdin.readLine());
		
		newNode.setPerformance(name);
		newNode.setName(lead);
		newNode.setParticipants(numParticipants);
		newNode.setLengthInMinutes(minutes);
		
		a.addAfterCurrent(newNode);
		menuDisplayAll(a);
		System.out.println("\n");
		String wait = stdin.readLine();
		
	}
	/**
	 * Adds a new item to the list at the end and sets the cursor and the tail
	 * as the newly added item.
	 * @param a
	 * 		the linked list of performance nodes
	 * postconditions: the cursor and tail will now equal the new node.
	 * @throws IOException
	 * 		if input is invalid.
	 */
	private static void menuAdd(PerformanceList a) throws IOException {
		InputStreamReader inStream = new InputStreamReader(System.in);
		BufferedReader stdin = new BufferedReader(inStream);
		
		PerformanceNode newNode = new PerformanceNode();
		String name = new String("");
		String lead = new String("");
		int minutes = 0;
		int numParticipants = 0;
		System.out.print("Enter performance name: ");
		name = stdin.readLine();
		System.out.print("Enter performance lead: ");
		lead = stdin.readLine();
		System.out.print("Enter number of performers: ");
		numParticipants = Integer.parseInt(stdin.readLine());
		System.out.print("Enter performance length: ");
		minutes = Integer.parseInt(stdin.readLine());
		
		newNode.setPerformance(name);
		newNode.setName(lead);
		newNode.setParticipants(numParticipants);
		newNode.setLengthInMinutes(minutes);
		
		a.addToEnd(newNode);
		menuDisplayAll(a);
		System.out.println("\n");
		String wait = stdin.readLine();
	}
}
