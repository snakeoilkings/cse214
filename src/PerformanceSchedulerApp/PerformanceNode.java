package PerformanceSchedulerApp;
/**
 * PerformanceNode class creates the PerformanceNode item to be implemented
 * in a linked list.
 * 
 * Homework 2
 * CSE 214
 * Recitation #6
 * TA: Kevin Flanygolts
 * Grader: Zheyuan (Jeffrey) Gao
 * 
 * @author Kathryn Blecher
 * email: kathryn.blecher@gmail.com
 * Stony Brook ID: 108871623
 */ class PerformanceNode {
	
	private String performance;     //name of the performance
	private String name;		    //name of the lead performer of performance
	private int participants;	    //number of participants in the performance
	private int lengthInMinutes;    //length of time of the performance
	
	private PerformanceNode next;   //the next PerformanceNode within a list
	private PerformanceNode prev;   //the previous PerformanceNode in a list
	
	private int startInMinutes = 0; //the start time of a set
	
	//Invariants:
	//startInMinutes is nonnegative.
	
	/**
	 * Creates a new performance node. Empty constructor
	 */
	public PerformanceNode() {
		performance = "";
		name = "";
		participants = 0;
		lengthInMinutes = 0;
	}
	/**
	 * Creates a new performance node. Overloaded Constructor;
	 * @param performance 
	 * 		Name of performance
	 * @param name 
	 * 		Name of lead performer
	 * @param participants 
	 * 		Number of participants
	 * @param lengthInMinutes 
	 * 		Length of the set
	 * Preconditions: participants and length can not be less than 0.
	 * @throws IllegalArgumentException 
	 * 		If participants or lengthInMinutes is less than 0
	 */
	public PerformanceNode(String performance, String name, int participants,
			int lengthInMinutes) throws IllegalArgumentException {
		if (participants < 0)
			throw new IllegalArgumentException("There can not be less than 0"
			  + " participants!");
		if (lengthInMinutes < 0)
			throw new IllegalArgumentException("The performance can not take "
			  + "less than 0 minutes to be performed!");
		this.performance = performance;
		this.name = name;
		this.participants = participants;
		this.lengthInMinutes = lengthInMinutes;
	}
	/**
	 * Mutator method for the performance name
	 * @param performance 
	 * 		The user input of performance name
	 * Preconditions: That the PerformanceNode has been instantiated
	 * Postconditions: performance is updated to user input
	 */
	public void setPerformance(String performance) {
		this.performance = performance;
	}
	/**
	 * Accessor method for performance name
	 * @return 
	 * 		name of performance
	 */
	public String getPerformance() {
		return performance;
	}
	/**
	 * Mutator method for name of lead performer
	 * @param name
	 * 		The user input of lead performer's name
	 * Preconditions: That the PerformanceNode has been instantiated
	 * Postconditions: name is updated to user input
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * Accessor method for name of lead performer
	 * @return 
	 * 		name of the lead performer
	 */
	public String getName()
	{
		return name;
	}
	/**
	 * Mutator method for number of participants
	 * @param participants
	 * 		The user input for the number of participants
	 * Preconditions: That the PerformanceNode has been instantiated, and
	 * 		number of participants is at least 0 or more 
	 * Postconditions: participants is updated to user input
	 * @throws IllegalArgumentException 
	 * 		if participants are less than 0
	 */
	public void setParticipants(int participants) throws 
			IllegalArgumentException{
		if (participants < 0)
			throw new IllegalArgumentException("There can not be less than 0 "
					  + "participants!");
		this.participants = participants;
	}
	/**
	 * Accessor method for the number of participants
	 * @return 
	 * 		number of participants
	 */
	public int getParticipants() {
		return participants;
	}
	/**
	 * Mutator method for the length of the performance
	 * @param lengthInMinutes
	 * 		The user input for the length of a performance
	 * Preconditions: That the PerformanceNode has been instantiated
	 * Postconditions: lengthInMinutes is updated to user input
	 */
	public void setLengthInMinutes(int lengthInMinutes) {
		this.lengthInMinutes = lengthInMinutes;
	}
	/**
	 * Accessor method for the number of minutes until the performance starts
	 * @return number of minutes until the performance's start
	 */
	public int getStartInMinutes() {
		return startInMinutes;
	}
	/**
	 * Mutator method for the start time of the performance
	 * @param startInMinutes
	 * 		The calculated start time of the performance based on linked list
	 * Preconditions: That the PerformanceNode has been instantiated
	 * Postconditions: startInMinutes is updated to proper time in list
	 */
	public void setStartInMinutes(int startInMinutes) {
		this.startInMinutes = startInMinutes;
	}
	/**
	 * Accessor method for length in minutes of the performance
	 * @return 
	 * 		number of minutes a performance lasts
	 */
	public int getLengthInMinutes() {
		return lengthInMinutes;
	}
	/**
	 * Mutator method for the next node in a list
	 * @param next
	 * 		The node that this PerformanceNode will link to
	 * Preconditions: That the PerformanceNode has been instantiated
	 * Postconditions: next is updated to new PerformanceNode
	 */
	public void setNext(PerformanceNode next) {
		this.next = next;
	}
	/**
	 * Accessor method for the next PerformanceNode in a list
	 * @return
	 * 		the next PerformanceNode in a linked list or null if none exist
	 */
	public PerformanceNode getNext() {
		return next;
	}
	/**
	 * Mutator method for the previous node in a list
	 * @param prev
	 * 		The node that this PerformanceNode will be linked from
	 * Preconditions: That the PerformanceNode has been instantiated
	 * Postconditions: prev is updated to new PerformanceNode
	 */
	public void setPrev(PerformanceNode prev) {
		this.prev = prev;
	}
	/**
	 * Accessor method for the previous PerformanceNode in a list
	 * @return 
	 * 		the previous PerformanceNode in a linked list or null if none exist
	 */
	public PerformanceNode getPrev() {
		return prev;
	}
	/**
	 * 
	 */
	public void setTime() {
		if(prev != null)
			startInMinutes= prev.getLengthInMinutes()+prev.getStartInMinutes();
		else
			startInMinutes = 0;
	}
	/**
	 * Creates a neat String of the data within the PerformanceNode for output
	 * @return
	 * 		A formatted string that displays all the PerformanceNode data
	 */
	@Override
	public String toString() {
		String output = new String("");
		output = String.format("%-2s%-25s%-27s%-17s%-15s%-6s","",performance,
				name,participants,lengthInMinutes,startInMinutes);
		return output;
	}
}
