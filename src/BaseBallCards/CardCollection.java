package BaseBallCards;
/**
 * 
 * @author Kathryn Blecher
 * 108871623
 * Homework 1
 * CSE 214
 * Recitation #6
 * TA: Kevin Flanygolts
 * Zheyuan (Jeffrey) Gao 
 * kathryn.blecher@gmail.com
 * @author Kathryn
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CardCollection {
	
	private static final int MAX_CARDS = 100;
	private BaseballCard[] collection;
	private int size;

	
	/**
	 * Creates an empty list of 100 baseball cards.
	 */
	public CardCollection() { 
		this.collection = new BaseballCard[MAX_CARDS];
		size = 0;
	}
	/**
	 * Gets the current size of a CardCollection	
	 * @return the amount of cards that are currently in the collection as int
	 */
	public int size() {
		return size;
	}
	/**
	 * Adds a BaseballCard to a collection at the given position
	 * @param newCard new baseball card being added
	 * @param position the position to put the card in
	 * preconditions: The collection in not currently full
	 * postconditions: the size variable is increased by 1
	 * @throws FullCollectionException
	 */
	public void addCard(BaseballCard newCard, int position) throws 
		FullCollectionException {
		
		if ((position < 1) || (position > (size() + 1)))
			throw new IllegalArgumentException("Not a valid position (enter 1-"
		+size()+ ")");
		if (size() == MAX_CARDS)
			throw new FullCollectionException("The list has reached the"
					+ " maximum of 100 cards. No more can be added.");
		
		for(int i = size(); i > position-1; i--) 
			collection[i] = (BaseballCard) collection[i-1].clone();
		collection[position-1] = newCard;
		
		size++;
		
		}
	/**
	 * Adds a BaseballCard to the collection at the end. Overloaded method
	 * @param newCard is new card being added
	 * preconditions: the collection is not full
	 * postconditions: the size variable is increased by 1
	 * @throws FullCollectionException
	 */
	public void addCard(BaseballCard newCard) throws FullCollectionException {
		addCard(newCard, size()+1);
	}
	/**
	 * removes a card from the collection
	 * preconditions: a card must exist in the collection
	 * postconditions: the size variable is decreased by 1
	 * @param position the position of the card to be removed
	 */
	public void removeCard(int position) {
		if ((position < 1) || (position > (size())))
			throw new IllegalArgumentException("Not a valid position (enter 1-"
					+ size() +")");
		
		for(int i = position-1; i < size()-1; i++) 
			collection[i] = (BaseballCard) collection[i+1].clone();
		
		size--;
	}
	/**
	 * Gets a card from the collection at the given position
	 * @param position the index of the card
	 * @return the card as a BaseballCard object
	 */
	public BaseballCard getCard(int position) {
		if ((position < 1) || (position > size()))
			throw new IllegalArgumentException("Not a valid position (enter 1-"
					+ size() +")");
		
		return collection[position-1];
	}
	/**
	 * Trades one card from the collection to another or itself
	 * @param other the collection you are trading with
	 * @param myPosition the index of the card you are trading
	 * @param theirPosition the index of the card they are trading
	 * preconditions: collection can not be full
	 * @throws FullCollectionException
	 */
	public void trade(CardCollection other, int myPosition, int theirPosition) 
			throws FullCollectionException {
		
		BaseballCard temp = new BaseballCard();
		
		if ((myPosition < 1) || (myPosition > size()))
			throw new IllegalArgumentException("Not a valid position (enter 1-"
					+ size() +")");
		if ((theirPosition < 1) || (theirPosition > other.size()))
			throw new IllegalArgumentException("Not a valid position (enter 1-"
					+ size() +")");
		
		temp = (BaseballCard) getCard(myPosition).clone();
		collection[myPosition-1] = (BaseballCard) 
					other.getCard(theirPosition).clone();
		other.removeCard(theirPosition);
		other.addCard(temp, theirPosition);
		
		
	}
	/**
	 * determines if a card exists in a collection
	 * @param card the card being searched
	 * @return whether or not the card exists as a boolean
	 */
	public boolean exists (BaseballCard card) {
		for(int i=0; i < size(); i++) {
			if (collection[i].equals(card))                                                                                                                                                                                                                                                             
				return true;
		}
		return false;
	}
	/**
	 * neatly prints out all of the cards in a collection
	 */
	public void printAllCards() {
		System.out.print(toString());
	}
	/**
	 * creates a string value displaying all baseballcards in a collection
	 * @return the string of all the baseball cards in columns
	 */
	@Override
	public String toString() {
		String[] cards = new String[size()];
		String output = new String("");
		String name, manufacturer;
		String year, size, price;
		output = String.format("%-4s%-16s%-15s%-12s%-10s%-1s","#","Name",
				"Manufacturer", "Year", "Price", "Size");
		output = output + "\n" + String.format("%-4s%-16s%-15s%-12s%-10s%-1s","--",
				"----","------------", "----", "------", "----") + "\n";
		for(int i=0; i < size(); i++) {
			name = (i+1) + ". " + collection[i].getName();
			manufacturer = collection[i].getManufacturer();
			year = String.valueOf(collection[i].getYear());
			price = formatPrice(collection[i].getPrice());
			size = String.valueOf(collection[i].getSizeX()) +"x"+ 
					String.valueOf(collection[i].getSizeY());
			cards[i] = String.format("%-20s%-15s%-10s%-10s%-20s",name,
					manufacturer,year,price,size);
			output = output + cards[i] + "\n";
		}
		return output;
		
	}
	/**
	 * makes the price print prettier based on amount of digits
	 * @param price the card
	 * @return a formatted string of the price
	 */
	public String formatPrice(double price)
	{
		if (price < 10)
			return "$   " + price;
		if (price < 100)
			return "$  " + price;
		if (price < 1000)
			return "$ " + price;
		return "$" + price;
	}
}

	