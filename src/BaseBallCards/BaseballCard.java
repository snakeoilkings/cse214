package BaseBallCards;
/**
 * 
 * @author Kathryn Blecher
 * 108871623
 * kathryn.blecher@gmail.com
 * Homework 1
 * CSE 214
 * Recitation #6
 * Kevin Flanygolts
 * Zheyuan (Jeffrey) Gao 
 * @author Kathryn
 */

public class BaseballCard implements Cloneable {
	
	private String name;
	private String manufacturer;
	private int year;
	private double price;
	private int[] size = new int[2];
	/**
	 * Creates a new BaseballCard object with blank parameters
	 */
	public BaseballCard() {}
	/**
	 * Creates a new BaseballCard object with given values. Overloaded 
	 * constructor.
	 * @name first and last name of player
	 * @param manufacturer card manufacturer
	 * @param year card year
	 * @param price dollar price of card
	 * @param sizeX width of card
	 * @param sizeY height of card
	 * preconditions: sizes are above 0, year is above 1900 or below 2016, and
	 * the price is nonnegative
	 * postconditions: BaseballCard object created with user values
	 * @throws Exception if price or size is negative, or if the year isn't
	 * within range
	 */
	public BaseballCard(String name, String manufacturer, int year, 
			double price, int sizeX, int sizeY) throws IllegalArgumentException {
		if (price < 0)
			throw new IllegalArgumentException("A card can not have a price "
					+ "below $0.00");
		if ((year < 1900) || (year > 2015))
			throw new IllegalArgumentException("The year is not valid");
		if ((sizeX < 1) || (sizeY < 1))
			throw new IllegalArgumentException("The size is not valid");
		this.name = name;
		this.manufacturer = manufacturer;
		this.year = year;
		this.price = price;
		this.size[0] = sizeX;
		this.size[1] = sizeY;
	}
	/**
	 * Creates a new BaseballCard object with given values but with an array
	 * for size instead of separate variables for x and y. Overloaded 
	 * constructor.
	 * @param name first and last name of player
	 * @param manufacturer card manufacturer
	 * @param year card year
	 * @param price dollar price of card
	 * @param size[] width and height of card
	 * preconditions: sizes are above 0, year is above 1900 or below 2016, and
	 * the price is nonnegative
	 * @throws Exception if price or size is negative, or if the year isn't
	 * within range
	 */
	public BaseballCard(String name, String manufacturer, int year, 
			double price, int[] size) throws IllegalArgumentException {
		if (price < 0)
			throw new IllegalArgumentException("A card can not have a price "
					+ "below $0.00");
		if ((year < 1900) || (year > 2015))
			throw new IllegalArgumentException("The year is not valid");
		if ((size[0] < 1) || (size[1] < 1))
			throw new IllegalArgumentException("The size is not valid");
		this.name = name;
		this.manufacturer = manufacturer;
		this.year = year;
		this.price = price;
		this.size = size;
	}
	/**
	 * Sets name of a BaseballCard
	 * @param name the new name it will become
	 */
	public void setName(String name) {
		this.name = name;
	}
	/** 
	 * Gets the name of a BaseballCard
	 * @return baseball card's name as a String
	 */
	public String getName() {
		return name;
	}
	/**
	 * Sets the name of the manufacturer
	 * postconditions: changes name of the card to user input
	 * @param manufacturer name of new manufacturer
	 */
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	/**
	 * Gets the name of a BaseballCard manufacturer
	 * postconditions: manufacturer changed to user input
	 * @return the name of manufacturer as a String
	 */
	public String getManufacturer() {
		return manufacturer;
	}
	/**
	 * Sets the year of the BaseballCard object
	 * @param year the year of the BaseballCard as an integer
	 * preconditions: the year is not negative
	 * postconditions: changes the year to user input
	 * @throws Exception if price or size is negative, or if the year isn't
	 * within range
	 */
	public void setYear(int year) throws IllegalArgumentException {
		if (price < 0)
			throw new IllegalArgumentException("A card can not have a price "
		      + "below $0.00");
		this.year = year;
	}
	/**
	 * Gets the year of the BaseballCard object
	 * @return the year of the card 
	 */
	public int getYear() {
		return year;
	}
	
	/**
	 * Sets the price of the BaseballCard object
	 * @param price price of BaseballCard
	 * Preconditions: The price is not negative.
	 * postconditions: changes price to user input
	 * @throws IllegalArgumentException
	 */
	
	public void setPrice(double price) {
		if (price < 0)
			throw new IllegalArgumentException("A card can not have a price "
					+ "below $0.00");
		this.price = price;
	}
	
	/**
	 * Gets the price of the BaseballCard object
	 * @return the price as a double
	 */
	public double getPrice() {
		return price;
	}
	
	/**
	 * Sets BaseballCard width
	 * postconditions: changes the width of the card to user input
	 * @param sizeX the width of the card
	 */
	public void setSizeX(int sizeX) {
		this.size[0]=sizeX;
	}
	
	/**
	 * Gets the BaseballCard width
	 * @return the width of the card as an int
	 */
	public int getSizeX() {
		return size[0];
	}
	/**
	 * Sets the height of the card
	 * postconditions: changes the height of the card to user input
	 * @param sizeY card height
	 */
	public void setSizeY(int sizeY) {
		this.size[1] = sizeY;
	}
	/**
	 * Gets the height of the BaseballCard
	 * @return the card height
	 */
	public int getSizeY() {
		return size[1];
	}
	/**
	 * Creates a new instance of an existing card.
	 * postconditions: a new instance of the card is created
	 * @return a clone of the BaseballCard
	 */
	@Override
	public Object clone() {
		BaseballCard copyCard = new BaseballCard();
		copyCard.setName(this.getName());
		copyCard.setManufacturer(this.getManufacturer());
		copyCard.setPrice(this.getPrice());
		copyCard.setYear(this.getYear());
		copyCard.setSizeX(this.getSizeX());
		copyCard.setSizeY(this.getSizeY());
		return copyCard;
	}
	/**
	 * Checks to see if two BaseballCards are identical
	 * @return whether they are the same or not as boolean
	 * */
	public boolean equals(Object otherCard) {
		if (!(otherCard instanceof BaseballCard))
			return false;
		if (!(otherCard instanceof BaseballCard))
			return false;
		if (this.getName() != ((BaseballCard) otherCard).getName())
			return false;
		if (this.getManufacturer() != ((BaseballCard) otherCard).getManufacturer())
			return false;
		if (this.getPrice() != ((BaseballCard) otherCard).getPrice())
			return false;
		if (this.getYear() != ((BaseballCard) otherCard).getYear())
			return false;
		if (this.getSizeX() != ((BaseballCard) otherCard).getSizeX())
			return false;
		if (this.getSizeY() != ((BaseballCard) otherCard).getSizeY())
			return false;
		return true;
	}
	/**
	 * Creates a string identifying a BaseballCards information
	 * @return the BaseballCards information as a String
	 */
	public String toString() {
		return name +"\n"+ manufacturer +"\n"+ year +"\n"+ price +"\n"+ size[0]
				+"x"+ size[1]+ "\n";
	}
	
	public static void main(String[] args)
	{
		BaseballCard JoeShmo = new BaseballCard("Joe Shmo", "Topps", 2010, 1.19, 4, 65);
		BaseballCard JohnShmo = new BaseballCard("Joe Shmo", "Topps", 2010, 1.19, 6, 6);
		BaseballCard LukeShmo = new BaseballCard();
		LukeShmo = (BaseballCard) JohnShmo.clone();
		System.out.println("The two cards are equal. This is " + ((LukeShmo.equals(JohnShmo) ? "true" : "false")));
		System.out.println(JoeShmo);
		int num = 27;
		System.out.print(LukeShmo.equals(num));
	}

}
