package BaseBallCards;
/**
 * 
 * @author Kathryn Blecher
 * 108871623
 * kathryn.blecher@gmail.com
 * Homework 1
 * CSE 214
 * Recitation #6
 * Kevin Flanygolts
 * Zheyuan (Jeffrey) Gao 
 * @author Kathryn
 */
public class FullCollectionException extends Exception {
	
	public FullCollectionException() {}
	
	public FullCollectionException(String message) { 
		super(message);
	}
}
