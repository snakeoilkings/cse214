package BaseBallCards;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class CollectionManager {
	/**
	 * test run with dummy data
	 */
	public static void main(String[] args) throws IOException, 
	FullCollectionException {
		InputStreamReader inStream = new InputStreamReader(System.in);
		BufferedReader stdin = new BufferedReader(inStream);
		
		String[] names = new String[20];
		CardCollection[] collections = new CardCollection[20];
		
		BaseballCard a = new BaseballCard("jj", "Topps", 2010, 1.19, 100, 200);
		BaseballCard b = new BaseballCard("jd", "Topps", 2010, 2.19, 100, 180);
		BaseballCard c = new BaseballCard("xc", "Idin", 1999, 0.19, 100, 200);
		BaseballCard d = new BaseballCard("rl", "News", 2014, 0.79, 200, 200);
		BaseballCard e = new BaseballCard("ks", "Topps", 2013, 2.45, 100, 200);
		CardCollection newSet = new CardCollection();
		CardCollection otherSet = new CardCollection();
		try {
		newSet.addCard(a); newSet.addCard(b); newSet.addCard(c); 
		otherSet.addCard(d); otherSet.addCard(e);
		} catch (FullCollectionException e1) { e1.printStackTrace(); }
		
		collections[0] = newSet;
		collections[1] = otherSet;
		names[0] = "New Set";
		names[1] = "Original Set";
		char choice = 'b';
		newSet.removeCard(2);
		
		while (choice != Character.toLowerCase('q')) {
			System.out.println("Main menu:" +"\n"+ "A) Add Card" +"\n"+ 
			  "C) Copy "
			  + "Card" +"\n"+ "E) Update Price" +"\n"+ "L) Look for Card" +"\n" 
			  + "N) Update name" +"\n"+ "P) Print All"
			  + "\n"+ "R) Remove Card" +"\n"+ "S) Size" +"\n" 
			  + "T) Trade" +"\n"+ "Q) Quit" + "\n");
			System.out.print("Select an option: ");
			choice = (char) System.in.read();
			String syntax = stdin.readLine();
			switch(choice)
				{
			case 'a': ;
			case 'A': menuAdd(collections, names);
				      break;
			case 'c': ;
			case 'C': menuCopy(collections, names);
				      break;
			case 'e': ;
			case 'E': menuPriceUpdate(collections, names);
				      break;
			case 'l': ;
			case 'L': menuGetCard(collections, names);
					  break;
			case 'n': ;
			case 'N': menuNameUpdate(collections, names);
				  	  break; 
			case 'p': ;
			case 'P': menuPrint(collections, names);
					  break;
			case 'r': ;
			case 'R': menuRemove(collections, names);
				  	  break;
			case 's': ;
			case 'S': menuSize(collections, names);
				  	  break;
			case 't': ;
			case 'T': menuTrade(collections, names);
				  	  break;
			case 'v':  ;
			case 'V':  menuValue(collections, names);
					  break;
			
			case 'q': ;
			case 'Q': return;
			}
		}
	}
	/** 
	 * Gets the position of a card in a collection
	 * @param list the collection to be searched
	 * @param name the card name to find
	 * @return either the position of -1 as a flag that it doesn't exist
	 * @throws IOException
	 */
	public static int menuLocate(CardCollection list, String name) throws 
	IOException {
		
		for(int i=1; i < list.size()+1; i++) {
			if (name.equals(list.getCard(i).getName())) 
				return i;
			}
		return -1;
	}
	/**
	 * Prints out the size of a collection that the user requests
	 * @param list the list of collections within the program
	 * @param collectionName the list of names of the collections
	 * @throws IOException
	 */
	public static void menuValue(CardCollection[] list, String[]
			collectionName) throws IOException {
		
		int temp = findCollection(list, collectionName, false);
		if (temp == 100)
			return;
		double value = 0;
		for(int i=1; i < list[temp].size()+1; i++) 
			value = value + list[temp].getCard(i).getPrice();
		System.out.print("The value of collection '" + collectionName[temp]
				+ "' is " + value + ".\n\nPress any key to return"
				+ " to the previous menu.");
	}
	/**
	 * Removes a card from the selected collection
	 * @param list list of all collections in program
	 * @param collectionName list of all collection names in program
	 * preconditions: valid input
	 * postconditions: removes a card from the BaseballCollection
	 * @throws IOException
	 */
	public static void menuRemove(CardCollection[] list, String[]
			collectionName) throws IOException {
		
		int temp = findCollection(list, collectionName, false);
		if (temp == 100)
			return;
		
		InputStreamReader inStream = new InputStreamReader(System.in);
		BufferedReader stdin = new BufferedReader(inStream);
		
		System.out.print("\n\nEnter the card name which you want to remove: ");
		String name = stdin.readLine();
		boolean found = false;
		
		while (!found) {
		for(int i=1; i < list[temp].size()+1; i++) {
			if (name.equals(list[temp].getCard(i).getName())) {
				list[temp].removeCard(i);
				System.out.println("Card found. \n\n...\n\nCard Removed"  +
				"\n\n Press any key to return to the main menu.");
				found = true;
				i=list[temp].size();
				char enter = (char) System.in.read();
				return;
			}
			if ((i == list[temp].size()-1) && (!found)) {
				System.out.println("\nThe card you were looking for can not be"
						+ " found. Please enter a new name or type 'B' to "
						+ "return to the main menu: ");
				name = stdin.readLine();
				if (name.toLowerCase().equals("b"))
					return;
				}
			}
		}
		
	}
	/**
	 * prints the size of a collection
	 * @param list the list of all collections
	 * @param collectionName the list of all collection names
	 * preconditions: valid input
	 * @throws IOException
	 */
	public static void menuSize(CardCollection[] list, String[]
			collectionName) throws IOException {
		
		int temp = findCollection(list, collectionName, false);
		if (temp == 100)
			return;
		
		System.out.print("The size of collection '" + collectionName[temp]
				+ "' is " + list[temp].size() + ".\n\nPress any key to return"
				+ " to the previous menu.");
	}
	/**
	 * Trades one card in one collection with another card in another 
	 * collection
	 * @param list the list of collections within the program
	 * @param collectionName list of all the names of the collections
	 * preconditions: the collection is not full and the input is correct
	 * @throws IOException
	 * @throws FullCollectionException
	 */
	public static void menuTrade(CardCollection[] list, String[]
			collectionName) throws IOException, FullCollectionException {
		
		InputStreamReader inStream = new InputStreamReader(System.in);
		BufferedReader stdin = new BufferedReader(inStream);
		
		System.out.println("\n\nSource collection:");
		int temp = findCollection(list, collectionName, false);
		if (temp == 100)
			return;
		
		System.out.print("Target collection:");
		int temp2 = findCollection(list, collectionName, false);
		if (temp2 == 100)
			return;
		
		String set1 = collectionName[temp];
		String set2 = collectionName[temp2];
		
		System.out.print("Enter the card from collection '" +set1+ "' that you"
				+ " would like to trade: ");
		String name = stdin.readLine();
		boolean found = false;
		
		while (!found) {
			int found1 = menuLocate(list[temp], name);
				if (found1 > 0) {
					System.out.println("Card found. \n\nEnter the card from "
						+ "collection '" +set2+ "' you want to trade: ");
					String name2 = stdin.readLine();
					int found2 = menuLocate(list[temp2], name2);
					if (found2 > 0) {
						System.out.println("Card found.\n\n...");
						list[temp].trade(list[temp2], found1, found2);
						System.out.print("\n\nTrade complete!\nPress any key "
							+ "to return to the main menu.");
						char enter = (char) System.in.read();
						return;
					}
					else {
						System.out.println("\nThe card you were looking for "
						  + "can not be found. Please enter a new name or type"
						  + " 'B' to return to the main menu: ");
					name = stdin.readLine();
					if (name2.toLowerCase().equals("b"))
						return;	
					}
				if (found1 < 0) {
					System.out.println("\nThe card you were looking for can "
						+ "not be found. Please enter a new name or type 'B' "
						+ "to return to the main menu: ");
					name = stdin.readLine();
					if (name.toLowerCase().equals("b"))
						return;
				}
			}
		}
	}
	/**
	 * prints a collection
	 * @param list list of all collections within the program
	 * @param collectionName list of all names of the collections
	 * preconditions: input is valid
	 * @throws IOException
	 */
	public static void menuPrint(CardCollection[] list, String[]
			collectionName) throws IOException
	{
		int temp = findCollection(list, collectionName, false);
		if (temp == 100)
			return;
		
		System.out.print(list[temp] + "\n\nPress any key to return to the "
				+ "previous menu.");
		char enter = (char) System.in.read();
		return;
		
	}
	/**
	 * Finds and prints a card
	 * @param list list of all the collections
	 * @param collectionName list of the collection names
	 * preconditions: input is valid
	 * @throws IOException
	 */
	public static void menuGetCard(CardCollection[] list, String[] 
			collectionName) throws IOException {
			
			int temp = findCollection(list, collectionName, false);
			if (temp == 100)
				return;
			
			String name = new String();
			String manufacturer = new String();
			InputStreamReader inStream = new InputStreamReader(System.in);
			BufferedReader stdin = new BufferedReader(inStream);
			
			BaseballCard newCard = new BaseballCard();
			System.out.print("Enter card name: ");
			name = stdin.readLine();
			System.out.print("Enter the manufacturer: ");
			manufacturer = stdin.readLine();
			System.out.print("Enter the year: ");
			int year = stdin.read();
			String syntax = stdin.readLine();
			System.out.print("Enter the price: ");
			double price = stdin.read();
			syntax = stdin.readLine();
			System.out.print("Enter the card width: ");
			int sizeX = stdin.read();
			syntax = stdin.readLine();
			System.out.print("Enter the card length: ");
			int sizeY = stdin.read();
			syntax = stdin.readLine();
			boolean found = false;
			
			newCard.setName(name);
			newCard.setManufacturer(manufacturer);
			newCard.setYear(year);
			newCard.setPrice(price);
			newCard.setSizeX(sizeX);
			newCard.setSizeY(sizeY);
			
			while (!found) {
			for(int i=1; i < list[temp].size()+1; i++) {
				if (newCard.equals(list[temp].getCard(i))) {
					System.out.println("Card found. \n\n"+list[temp].getCard(i)
					  +	"\n\n Press any key to return to the main menu.");
					found = true;
					i=list[temp].size();
					char enter = (char) System.in.read();
					return;
				}
				if ((i == list[temp].size()-1) && (!found)) {
					System.out.println("\nThe card you were looking for can "
					  + "not be found. Please enter a new name or type 'B' to "
					  + "return to the main menu: ");
					name = stdin.readLine();
					if (name.toLowerCase().equals("b"))
						return;
					}
				}
			}
		}
	/**
	 * The user adds a baseball card to the chosen collection
	 * @param list the list of collections in the program
	 * @param collectionName list of collection names
	 * preconditions: input is valid and collection is not full
	 * @throws IOException
	 * @throws FullCollectionException
	 */
	public static void menuAdd(CardCollection[] list, String[] 
			collectionName) throws IOException, FullCollectionException {
		
		int temp = findCollection(list, collectionName, true);
		if (temp == 100)
			return;

		String name = new String();
		String manufacturer = new String();
		InputStreamReader inStream = new InputStreamReader(System.in);
		BufferedReader stdin = new BufferedReader(inStream);
		
		BaseballCard newCard = new BaseballCard();
		System.out.print("Enter card name: ");
		name = stdin.readLine();
		System.out.print("Enter the manufacturer: ");
		manufacturer = stdin.readLine();
		System.out.print("Enter the year: ");
		int year = stdin.read();
		String syntax = stdin.readLine();
		System.out.print("Enter the price: ");
		double price = stdin.read();
		syntax = stdin.readLine();
		System.out.print("Enter the card width: ");
		int sizeX = stdin.read();
		syntax = stdin.readLine();
		System.out.print("Enter the card length: ");
		int sizeY = stdin.read();
		syntax = stdin.readLine();
		
		newCard.setName(name);
		newCard.setManufacturer(manufacturer);
		newCard.setYear(year);
		newCard.setPrice(price);
		newCard.setSizeX(sizeX);
		newCard.setSizeY(sizeY);
		
		CardCollection newSet = new CardCollection();
		newSet.addCard(newCard);
		list[temp] = newSet;
		
		System.out.println("\nSuccess! Card added to collection '" 
		+ collectionName[temp] + "'!");
		
	}
	/**
	 * Copies a card into another set 
	 * @param list the list of collections in the program
	 * @param collectionName list of names of the collections of the program
	 * preconditions: the input is valid and both collections aren't full
	 * postconditions: copies a card into another collection unless it is 
	 * full
	 * @throws IOException
	 * @throws FullCollectionException
	 */
	public static void menuCopy(CardCollection[] list, String[] 
			collectionName) throws IOException, FullCollectionException {
		
		InputStreamReader inStream = new InputStreamReader(System.in);
		BufferedReader stdin = new BufferedReader(inStream);
		
		System.out.println("\n\nSource collection:");
		int temp = findCollection(list, collectionName, false);
		if (temp == 100)
			return;
		
		System.out.print("Target collection:");
		int temp2 = findCollection(list, collectionName, false);
		if (temp2 == 100)
			return;
		
		String set1 = collectionName[temp];
		String set2 = collectionName[temp2];
		
		System.out.print("Enter the card position from collection '" +set1+ 
				"' that you"
				+ " would like to copy to '" +set2+ "': ");
		int found1 = stdin.read();
		
		list[temp2].addCard((BaseballCard)list[temp].getCard(found1).clone());
		System.out.println("Card found. \n\n...\n\nCard copy "
			+ "successful!");
	}
	/**
	 * Updates the price of a user selected collection
	 * @param list the list of all the collections in the program
	 * @param collectionName the list of collection names
	 * preconditions: the input is valid
	 * postconditions: changes the price of a card in a collection
	 * @throws IOException
	 */
	public static void menuPriceUpdate(CardCollection[] list, String[] 
			collectionName) throws IOException {
		
		int temp = findCollection(list, collectionName, false);
		if (temp == 100)
			return;
		
		InputStreamReader inStream = new InputStreamReader(System.in);
		BufferedReader stdin = new BufferedReader(inStream);
		System.out.print("\n\nEnter the card name which price you want to "
		  + "update: ");
		String name = stdin.readLine();
		boolean found = false;
		
		while (!found) {
		for(int i=1; i < list[temp].size()+1; i++) {
			if (name.equals(list[temp].getCard(i).getName())) {
				System.out.println("Card found. \n\n" +list[temp].getCard(i) +
				"\n\nEnter in the new price: ");
				double price = stdin.read();
				(list[temp].getCard(i)).setPrice(price);
				System.out.print("\n...\n\nCard successfully updated!");
				found = true;
				i=list[temp].size();
			}
			if ((i == list[temp].size()-1) && (!found)) {
				System.out.println("\nThe card you were looking for can not be"
						+ " found. Please enter a new name or type 'B' to "
						+ "return to the main menu: ");
				name = stdin.readLine();
				if (name.toLowerCase().equals("b"))
					return;
				}
			}
		}
	}
	/**
	 * Updates the Name of a user selected baseball card
	 * @param list the list of all the collections in the program
	 * @param collectionName the list of collection names
	 * preconditions: the input is valid
	 * postconditions: changes the Name of a card in a collection
	 * @throws IOException
	 */
	public static void menuNameUpdate(CardCollection[] list, String[] 
			collectionName) throws IOException {
		
		int temp = findCollection(list, collectionName, false);
		if (temp == 100)
			return;
		
		InputStreamReader inStream = new InputStreamReader(System.in);
		BufferedReader stdin = new BufferedReader(inStream);
		System.out.print("\n\nEnter the card name which name you want to "
		  + "update: ");
		String name = stdin.readLine();
		boolean found = false;
		
		while (!found) {
		for(int i=1; i < list[temp].size()+1; i++) {
			if (name.equals(list[temp].getCard(i).getName())) {
				System.out.println("Card found. \n\n" +list[temp].getCard(i) +
				"\n\nEnter in the new name: ");
				String name2 = stdin.readLine();
				(list[temp].getCard(i)).setName(name2);
				System.out.print("\n...\n\nCard successfully updated!");
				found = true;
				i=list[temp].size();
			}
			if ((i == list[temp].size()-1) && (!found)) {
				System.out.println("\nThe card you were looking for can not be"
						+ " found. Please enter a new name or type 'B' to "
						+ "return to the main menu: ");
				name = stdin.readLine();
				if (name.toLowerCase().equals("b"))
					return;
				}
			}
		}
	}
	/** This method finds a Baseball Collection within the main programs array
	 * and returns either a valid index or 100, which signals the method that 
	 * called it to return to the main menu.
	 * @param list list of all collections in the program
	 * @param collectionName list of all names of the collections
	 * @param acceptNew decides whether the user can make a new collection
	 * precondition: input is valid
	 * @return the index of the collection in the list as an int, or returns
	 * 100 if the user wants to return to the main menu, or -1 if it doesn't 
	 * exist
	 * @throws IOException
	 */
	
	public static int findCollection(CardCollection[] list, String[] 
			collectionName, boolean acceptNew) throws IOException {
		
		int check= -1;
		int temp = -1;
		String name = new String();
		String message = new String();
		InputStreamReader inStream = new InputStreamReader(System.in);
		BufferedReader stdin = new BufferedReader(inStream);
		
		for(int i=0; i < collectionName.length; i++) {
			if (collectionName[i] != null)
				check++;
		}
		
		if (check < 0) 
			System.out.print("No Collections Found.\n\n");
		else {
			message = "Enter the name of the collection" + ((acceptNew) ? 
				", or type 'N' to create a new collection: " : ": ");
		
			while ((check >= 0) && (temp < 0) && 
			!((name.toLowerCase()).equals("n"))) {
				System.out.print("\n\n" +message+ "\n");
				name = stdin.readLine();
				if (!((name.toLowerCase()).equals("n"))) {
					for(int i=0; i < check+1; i++)
						if (collectionName[i].equals(name))
							temp = i;
					if (temp < 0) {
						System.out.println("Collection not found. Press 'S' to"
							+" search again, " +((acceptNew) ? "'N' to create "
							+ "a new list,"	: "") + " or 'B' to return to the "
							+ "previous menu.");
			
						char choice = (char) System.in.read();
						if ((!acceptNew) && Character.toLowerCase(choice)=='n')
							choice = 's';
						switch(choice)
						{
							case 'b': ;
							case 'B': return 100;
					  
							case 's': ;
							case 'S': name = stdin.readLine();
									  break;
			
							case 'n': ;
							case 'N': temp = check;
						   	      	  check = -1;
						   	      	  break;
						}
					}
				}
			}
		}
		if ((check < 0) || (name.toLowerCase()).equals("n")) {
			boolean isValid = false;
			String newName = new String();
			temp = check;
			while (!isValid) {
				System.out.println("\n\nEnter a name of your new collection: "
						+ "");
				newName = stdin.readLine();
				isValid = true;
				for (int i=0; i < temp + 1; i++) {
					if(collectionName[i].equals(newName)) {
						isValid = false;
						i = collectionName.length;
					}
				if (!isValid) 
					System.out.println("The name you entered already exists.");
				}
			}
			collectionName[temp+1] = newName;
			return temp + 1;	
			}
		return temp;
	}
}

