package LibraryDatabase;
/**
 * HashedLibrary creates a hashtable of books and saves it in obj
 * 
 * Homework 6
 * CSE 214
 * Recitation #6
 * TA: Kevin Flanygolts
 * Grader: Zheyuan (Jeffrey) Gao
 * 
 * @author Kathryn Blecher
 * email: kathryn.blecher@gmail.com
 * Stony Brook ID: 108871623
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Scanner;

import core.data.DataSource;


public class HashedLibrary implements Serializable{
	
	private static int size;   //user defined start size of hashtable
	private static Hashtable<Integer, Book> table;  //hashtable library
	/**
	 * empty constructor
	 */
	public HashedLibrary() {
		this.size = 1000;
		this.table = new Hashtable<Integer, Book>(size);
	}
	/**
	 * Overloaded constructor
	 * @param size of starting hash
	 */
	public HashedLibrary(int size) {
		this.size = size;
		this.table = new Hashtable<Integer, Book>(size);
	}
	/**
	 * Adds a book to the hashtable, then serializes table to hash.obj
	 * @param title of book
	 * @param author of book
	 * @param publisher of book
	 * @param isbn of book
	 * @throws IOException from hashdata() if IOinput is invalid
	 */
	public static void addBook(String title, String author, String publisher, String
	  isbn) throws IOException {
		getHashData();
		boolean added = false;
		Book book = new Book(title, author, publisher, isbn);
		int key = (int) (Long.parseLong(isbn) % 983);
		if (table.containsKey(key))	{
			Book check = new Book();
			check = getBookByisbn(isbn);
		if (check!=null) 
			return;
		}
		while (!added) {
			if (!table.containsKey(key)) {
				table.put(key, book);
				added = true;
			}
			key++;
		}
		hashData();
	}
	/**
	 * Adds a list of books from a txt file with an XML list
	 * @param fileName name of txt file
	 * @throws IOException in addbook(...) >> gethash()
	 */
	public static void addAllBookInfo(String fileName) throws
	  IOException {
		String readName = new String("");
		String name = System.getProperty("java.class.path");
		String[] paths = name.split(";");
		name = paths[0] + "\\" +fileName;
		File file = new File(name);	
		if (!(file.exists())) {
			throw new FileNotFoundException("Can't Find File " + name + ". Try Again");
			}
		Scanner input = new Scanner(file);
		while(input.hasNext()) {
			readName = input.nextLine();
			DataSource ds = DataSource.connect("http://www.cs.stonybrook.edu/"
				+ "~cse214/hw/hw6/" + readName + ".xml").load();
			String title = ds.fetchString("title");
			String author = ds.fetchString("author");
			String publisher = ds.fetchString("publisher");
			String isbn = ds.fetchString("isbn");
			addBook(title, author, publisher, isbn);
		}
	}
	/**
	 * finds a book by hash key of isbn
	 * @param isbn (key)
	 * @return the book if found
	 */
	public static Book getBookByisbn(String isbn) {
		getHashData();
		int i=0;
		Book book = new Book();
		int key = (int) (Long.valueOf(isbn) % 983);
		while (table.containsKey(key)) {
			if (isbn.equals(table.get(key).getISBN())) 
				return table.get(key);
			key++;
			i++;
		}
		return null;
	}
	/**
	 * prints out the book library in a neat tabular form
	 */
	public void printCatalog() {
		getHashData();	
		String output = new String();
		String output2 = new String();
		output = String.format("%-20s%-40s%-25s%-25s","Book ISBN","Title","Author", "Publisher");
		System.out.println(output);
		System.out.println("--------------------------------------------------"
				+ "----------------------------------------------------------");
		String bookInfo = table.toString();
		bookInfo = bookInfo.replaceAll("[{][\\d]{3}=", "#%~~");
		bookInfo = bookInfo.replaceAll("[,][\\s][\\d]{3}=", "#%~~");
		String[] books = bookInfo.split("#%~~");
		for(int i=1; i<books.length;i++) {
			String[] books2 = books[i].split(", ");
			if (i==books.length-1)
				books2[3] = books2[3].substring(0,books2[3].length()-1);
			output2 = String.format("%-20s%-40s%-25s%-25s", books2[3], books2[0], books2[1], books2[2]);
			System.out.println(output2);
		}
		System.out.println();
	}
	/**
	 * serializes the data to an output file
	 */
	public static void hashData() {
		try {
			String name = System.getProperty("java.class.path");
			String[] paths = name.split(";");
			name = paths[0] + "\\" +"hash.obj";
	        FileOutputStream fileOut = new FileOutputStream(name);
	        ObjectOutputStream out = new ObjectOutputStream(fileOut);
	        out.writeObject(table);
	        out.close();
	        fileOut.close(); 
	    } catch(IOException i) { i.printStackTrace(); }
	}
	/**
	 * grabs serialized data from an input file
	 */
	private static void getHashData() {
		String name = System.getProperty("java.class.path");
		String[] paths = name.split(";");
		name = paths[0] + "\\" +"hash.obj";
		File file = new File(name);
		if (file.exists()) {
			try {
	    		FileInputStream fileIn = new FileInputStream(name);
	    		ObjectInputStream in = new ObjectInputStream(fileIn);
	        	table = (Hashtable) in.readObject();
	        	in.close();
	        	fileIn.close(); } 
	   		catch(IOException i) {
	         	i.printStackTrace();
	         	return; }
	    	catch(ClassNotFoundException c) {
	    	 	System.out.println("HashedLibrary class not found");
	         	c.printStackTrace();
	         	return;
	    	}
		}
	}
}