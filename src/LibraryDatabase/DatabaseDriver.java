package LibraryDatabase;
/**
 * Database Driver runs the library menu for user access
 * 
 * Homework 6
 * CSE 214
 * Recitation #6
 * TA: Kevin Flanygolts
 * Grader: Zheyuan (Jeffrey) Gao
 * 
 * @author Kathryn Blecher
 * email: kathryn.blecher@gmail.com
 * Stony Brook ID: 108871623
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.util.Hashtable;
import java.util.Scanner;


public class DatabaseDriver implements Serializable{
	
	public static void main(String[] args) throws IOException {
		InputStreamReader inStream = new InputStreamReader(System.in);
		BufferedReader stdin = new BufferedReader(inStream);
		HashedLibrary j = new HashedLibrary();
		char choice = ' ';
		while (choice != Character.toLowerCase('q')) {
			System.out.println("Main menu:" +"\n"+ "D) Display all books\n"
			  + "G) Look up book by ISBN\n"+ "L) Record all books from file\n"
			  + "R) Add a new book\n" + "Q) Quit\n");
			System.out.print("Select an option: ");
			choice = (char) System.in.read();
			String syntax = stdin.readLine();
			switch(choice)
				{
			case 'd': ;
			case 'D': j.printCatalog();
				      break;
			case 'g': ;
			case 'G': lookUp(j);
				      break;
			case 'l': ;
			case 'L': recordFromFile(j);
				      break;
			case 'r': ;
			case 'R': addNew(j);
					  break;
			case 'q': ;
			case 'Q': System.out.print("\nProgram Terminating...");
					  break;
			}
		}
	}
	/**
	 * adds books to database from a prompted user input file
	 * @param j the hashlibrary
	 * @throws IOException with invalid input data
	 */
	public static void recordFromFile(HashedLibrary j) throws IOException {
		Scanner input = new Scanner(System.in);
		System.out.print("\nPlease enter a text file to read: ");
		String fileName = input.next();
		j.addAllBookInfo(fileName);
	}
	/**
	 * adds a book to the database
	 * @param j hashlibrary
	 * @throws IOException with invalid data
	 */
	public static void addNew(HashedLibrary j) throws IOException {
		Scanner input = new Scanner(System.in);
		String title, author, publisher, isbn;
		System.out.print("\nEnter the book title: ");
		title = input.nextLine();
		System.out.print("\nEnter the book author: ");
		author = input.nextLine();
		System.out.print("\nEnter the book publisher: ");
		publisher = input.nextLine();
		System.out.print("\nEnter the book ISBN: ");
		isbn = input.nextLine();
		j.addBook(title, author, publisher, isbn);
		System.out.print("\n" +title+ " by " +author+ " has been added.\n\n");
	}
	/**
	 * finds a book by user prompted isbn
	 * @param j
	 */
	public static void lookUp(HashedLibrary j) {
		Scanner input = new Scanner(System.in);
		Book book = new Book();
		String isbn = new String();
		String output, output2;
		System.out.print("\nEnter an ISBN: ");
		isbn = input.nextLine();
		book = j.getBookByisbn(isbn);
		if (book==null) {
			System.out.print("\nBook not found, returning to main menu.\n\n");
			return;
		}
		output = String.format("%-20s%-40s%-25s%-25s","Book ISBN","Title",
		  "Author", "Publisher");
		System.out.println("\n" +output);
		System.out.println("--------------------------------------------------"
		  + "----------------------------------------------------------");
		String[] bookInfo = book.toString().split(", ");
		output2 = String.format("%-20s%-40s%-25s%-25s",bookInfo[3], bookInfo[0],
		  bookInfo[1], bookInfo[2]);
		System.out.println(output2+ "\n");	
	}
}
