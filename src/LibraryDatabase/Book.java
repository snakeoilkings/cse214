package LibraryDatabase;
/**
 * 
 * Book class creates Book object that stores title, author, isbn, & publisher
 * 
 * Homework 6
 * CSE 214
 * Recitation #6
 * TA: Kevin Flanygolts
 * Grader: Zheyuan (Jeffrey) Gao
 * 
 * @author Kathryn Blecher
 * email: kathryn.blecher@gmail.com
 * Stony Brook ID: 108871623
 */

import core.data.DataSource;
import java.io.Serializable;

public class Book implements Serializable {

	private String title = "";		//book title
	private String author = "";		//book author
	private String publisher = "";  //book publisher
	private String isbn = "";		//book isbn
	/**
	 * Empty constructor
	 */
	public Book() {
	}
	/**
	 * Overloaded constructor
	 * @param title of book
	 * @param author of book
	 * @param publisher of book
	 * @param isbn of book
	 */
	public Book(String title, String author, String publisher, String isbn) {
		this.title = title;
		this.author = author;
		this.publisher = publisher;
		this.isbn = isbn;
	}
	/**
	 * gets title
	 * @return title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * sets title to user specification
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * gets author
	 * @return author
	 */
	public String getAuthor() {
		return author;
	}
	/**
	 * sets author to user specifications
	 * @param author
	 */
	public void setAuthor(String author) {
		this.author = author;
	}
	/**
	 * gets publisher
	 * @return publisher
	 */
	public String getPublisher() {
		return publisher;
	}
	/**
	 * sets publisher to user specifications
	 * @param publisher
	 */
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	/**
	 * gets isbn
	 * @return isbn
	 */
	public String getISBN() {
		return isbn;
	}
	/**
	 * sets isbn to user specification
	 * @param isbn
	 */
	public void setISBN(String isbn) {
		this.isbn = isbn;
	}
	/**
	 * toString method creates a string of all 4 strings
	 * @return string
	 */
	public String toString() {
		return title +", "+ author +", "+ publisher +", "+ isbn;
	}
}
