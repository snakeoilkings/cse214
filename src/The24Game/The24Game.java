package The24Game;
import java.util.Scanner;

public class The24Game {
	
	public static void sort(double[] num)
	{
		double temp = 0;
		int j = 0;
		for(int i = 0; i < 7; i++){
			if(num[j] > num[j+1]){
				temp = num[j];
				num[j] = num[j+1];
				num[j+1] = temp;
			}
			j++;
			if (j % 3 == 0)
				j = 0;
		}
	}
	//prints appropriate operator after third number
	public static String printEq(int a)
	{
		if (a==0) return " * ";
		if (a==1) return " + ";
		if (a==2) return " / ";
		if (a==3) return " - ";
		if (a==4) return " / ";
		else      return " - ";
	}
	//prints appropriate operator after second number
	public static String printEq2(int a)
	{
		if (a<6)  return " * ";
		if (a<12) return " + ";
		if (a<18) return " / ";
		if (a<24) return " - ";
		if (a<30) return " / ";
		else      return " - ";
	}
	//prints appropriate operator following first number
	public static String printEq3(int a)
	{
		if (a<36)  return " * ";
		if (a<72)  return " + ";
		if (a<108) return " / ";
		if (a<144) return " - ";
		if (a<180) return " / ";
		else       return " - ";
	}
	
	//prints the numbers who match 24
	public static void check24(double[][][] num, double num1, double num2, 
			double num3, double num4)
	{
		for (int i = 0; i < 216; i++) {
			for (int j = 0; j < 36; j++) {
				for (int k = 0; k < 6; k++) {
					if ((num[i][j][k] >= 24 && num[i][j][k] < 24.01) || 
							(num[i][j][k] <= 24 && num[i][j][k] > 23.99)) {        
						if ((j >= 24) && (i < 144))
							System.out.println(num1 + printEq3(i) + "((" + 
							  ((k<4) ? num3+ printEq(k) + num4 : num4+ 
							  printEq(k) + num3) + ") " + printEq2(j) +num2+ 
								") = 24");
						else if ((j < 24) && (i < 144))
							System.out.println(num1 + printEq3(i)  + "(" +num2+ 
							  printEq2(j) + "(" +	((k<4) ? num3+ printEq(k) + 
							  num4 : num4+ printEq(k) + num3) + ")) = 24");
						else if ((j >= 24) && (i >= 144))
							System.out.println("((" + ((k<4) ? num3+ printEq(k)
							  +num4 : num4+ printEq(k) + num3) +")"+printEq2(j)
							  +num2+ ")" + printEq3(i) +num1+ " = 24");
						else
							System.out.println("(" +num2+ printEq2(j) + "(" + 
							  ((k<4) ? num3+ printEq(k)+num4 : num4+ printEq(k)
							  + num3) + "))" + printEq3(i) + num1+ " = 24");
					}
				}
			}
		}	
	}
	/*creates a final matrix from the matrix in check2 and the remaining number
	It checks every possible outcome between them and in the way the matrix is 
	constructed, I can tell the operators used between the numbers*/
	public static void check1(double num1,double num2,double num3, double num4)
	{
		int j = 0; int k = 0; int m = 0;
    	double[][][] c0 = new double[216][36][6];
		double[][] c1 = check2(num2, num3, num4);
			for (int i=0; i < 36; i++) {
				c0[i][i][k] =  num1 * c1[i][k];
				k++;
				if (k==6)
					k=0;
			}
			for (int i=36; i < 72; i++) {
				c0[i][j][k] =  num1 + c1[j][k];
				k++;
				if (k==6)
					k=0;
				j++;
				if (j==36)
					j=0;
			}		
			for (int i=72; i < 108; i++) {
				c0[i][j][k] =  num1 / c1[j][k];
				k++;
				if (k==6)
					k=0;
				j++;
				if (j==36)
					j=0;
			}	
			for (int i=108; i < 144; i++) {
				c0[i][j][k] =  num1 - c1[j][k];
				k++;
				if (k==6)
					k=0;
				j++;
				if (j==36)
					j=0;
			}	
			for (int i=144; i < 180; i++) {
				c0[i][j][k] =  c1[j][k] / num1;
				k++;
				if (k==6)
					k=0;
				j++;
				if (j==36)
					j=0;
			}	
			for (int i=180; i < 216; i++) {
				c0[i][j][k] =  c1[j][k] - num1;
				k++;
				if (k==6)
					k=0;
				j++;
				if (j==36)
					j=0;
			}	
		check24(c0, num1, num2, num3, num4);
	}
	/*creates a matrix from an array created in check3 and another number which
	finds every possible outcome between the 2. by doing this I can tell which
	operator was used in the original array*/
	public static double[][] check2(double num2, double num3, double num4)
	{										
		double[] c3 = check3(num3, num4);
		double[][] c2 = new double[36][6];
		int j = 0;
		for (int i = 0; i < 6; i++) 
			c2[i][i] = num2 * c3[i];
		for (int i = 6; i < 12; i++) {
			c2[i][j] = num2 + c3[j];
			j++;
	    }
		j=0;
		for (int i = 12; i < 18; i++) {
			c2[i][j] = num2 / c3[j];
			j++;
		}
		j=0;
		for (int i = 18; i < 24; i++) {
			c2[i][j] = num2 - c3[j];
			j++;
		}
		j=0;
		for (int i = 24; i < 30; i++) {
			c2[i][j] = c3[j] / num2;
			j++;
		}
		j=0;
		for (int i = 30; i < 36; i++) {
			c2[i][j] = c3[j] - num2;
			j++;
		}
		return c2;
	}
	
	//saves in an array every possible outcome between two numbers
	public static double[] check3(double num3, double num4)  
	{											
		double[] c3 = new double[6];
		c3[0] = num3 * num4;
		c3[1] = num3 + num4;
		c3[2] = num3 / num4;
		c3[3] = num3 - num4;
		c3[4] = num4 / num3;
		c3[5] = num4 - num3;
		return c3;
	}
	
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		System.out.print("Enter 4 integers 1 - 9: ");
		double[] number = new double[4];
		int p = 0;
		while(p < 4) {
			number[p] = input.nextDouble();
			p++;
		}
		
		input.close();
		
		check1(number[0], number[1], number[2], number[3]);
		check1(number[0], number[2], number[1], number[3]);
		check1(number[0], number[3], number[2], number[1]);
		check1(number[1], number[0], number[2], number[3]);
		check1(number[1], number[2], number[0], number[3]);
		check1(number[1], number[3], number[2], number[0]);
		check1(number[2], number[3], number[1], number[0]);
		check1(number[2], number[1], number[0], number[3]);
		check1(number[2], number[0], number[1], number[3]);
		check1(number[3], number[0], number[1], number[2]);
		check1(number[3], number[1], number[0], number[2]);
		check1(number[3], number[2], number[1], number[0]);
		
		sort(number);
		
		double[] ab = check3(number[0], number[1]);
		double[] bc = check3(number[1], number[2]);
		double[] cd = check3(number[2], number[3]);
		double[] ac = check3(number[0], number[2]);
		double[] ad = check3(number[0], number[3]);
		double[] bd = check3(number[1], number[3]);
		
		for(int i = 0; i < 6; i++)
		{
			for(int j = 0; j < 6; j++)
			{
				if ((ab[i] + cd[j]) == 24) {
					if (i >= 4)
						System.out.print("("+number[1] +printEq(i)+ number[0]);
					else
						System.out.print("("+ number[0] +printEq(i)+number[1]);	
					if (j >= 4)
						System.out.println(") + (" + number[3] +printEq(j)+ 
						  number[2] + ") = 24");
					else
						System.out.println(") + (" + number[2] + printEq(j) + 
						  number[3] + ") = 24");
				}
				if ((ab[i] - cd[j]) == 24) {
					if (i >= 4)
						System.out.print("("+number[1] +printEq(i)+ number[0]);
					else
						System.out.print("("+number[0] +printEq(i)+ number[1]);
					if (j >= 4)
						System.out.println(") - (" + number[3] +printEq(j)+
						  number[2] + ") = 24");
					else
						System.out.println(") - (" + number[2] +printEq(j)+ 
						  number[3] + ") = 24");
				}
				if ((ab[i] / cd[j]) == 24) {
					if (i >= 4)
						System.out.print("("+number[1] +printEq(i)+ number[0]);
					else
						System.out.print("("+number[0] +printEq(i)+ number[1]);	
					if (j >= 4)
						System.out.println(") / ("+ number[3] +printEq(j)+ 
						  number[2] + ") = 24");
					else
						System.out.println(") / ("+ number[2] +printEq(j)+ 
						  number[3] + ") = 24");
				}
				if ((ab[i] * cd[j]) == 24) {
					if (i >= 4)
						System.out.print("(" +number[1]+printEq(i)+ number[0]);	
					else
						System.out.print("(" +number[0]+printEq(i)+ number[1]);	
					if (j >= 4)
						System.out.println(") * ("+ number[3] + printEq(j) + 
						  number[2] + ") = 24"); 
					else
						System.out.println(") * ("+ number[2] + printEq(j) + 
						 number[3] + ") = 24"); 
					}
				if ((ac[i] + bd[j]) == 24) {
					if (i >= 4)
						System.out.print("(" +number[0]+printEq(i)+ number[2]);	
					else
						System.out.print("("+number[2] +printEq(i)+ number[0]);	
					if (j >= 4)
						System.out.println(") + (" + number[3] + printEq(j) + 
						  number[1] + ") = 24");
					else
						System.out.println(") + (" + number[1] + printEq(j) + 
						  number[3] + ") = 24");
					}
				if ((ac[i] - bd[j]) == 24) {
					if (i >= 4)
						System.out.print("("+number[2] +printEq(i)+ number[0]);	
					else
						System.out.print("("+number[0] +printEq(i)+ number[2]);	
					if (j >= 4)
						System.out.println(") - ("+ number[3] + printEq(j) + 
						  number[1] + ") = 24");
					else
						System.out.println(") - ("+ number[1] + printEq(j) + 
						  number[3] + ") = 24");
					}
				if ((ac[i] / bd[j]) == 24) {
					if (i >= 4)
						System.out.print("("+number[2] +printEq(i)+ number[0]);	
					else
						System.out.print("("+number[0] +printEq(i)+ number[2]);	
					if (j >= 4)
						System.out.println(") / ("+ number[3] + printEq(j) + 
						  number[1] + ") = 24");
					else
						System.out.println(") / ("+ number[1] + printEq(j) + 
						  number[3] + ") = 24");
				}
				if ((ac[i] * bd[j]) == 24) {
					if (i >= 4)
						System.out.print("("+number[2] +printEq(i)+ number[0]);
					else
						System.out.print("("+number[0] +printEq(i)+ number[2]);	
					if (j >= 4)
						System.out.println(") * ("+ number[3] + printEq(j) 
						  + number[1] + ") = 24");
					else
						System.out.println(") * ("+ number[1] + printEq(j) 
						  + number[3] + ") = 24");
				}
				if ((ad[i] + bc[j]) == 24) {
					if (i >= 4)
						System.out.print("("+number[0] +printEq(i)+ number[3]);	
					else
						System.out.print("("+number[3] +printEq(i)+ number[0]);	
					if (j >= 4)
						System.out.println(") + (" + number[2] +printEq(j)+ 
						  number[1] + ") = 24");
					else
						System.out.println(") + (" + number[1] +printEq(j)+ 
						  number[2] + ") = 24");
					}
				if ((ad[i] - bc[j]) == 24) {
					if (i >= 4)
						System.out.print("("+number[3] +printEq(i)+ number[0]);	
					else
						System.out.print("("+number[0] +printEq(i)+ number[3]);	
					if (j >= 4)
						System.out.println(") - ("+number[2] +printEq(j)+ 
						  number[1] + ") = 24");
					else
						System.out.println(") - ("+ number[1] + printEq(j) + 
						  number[2] + ") = 24");
					}
				if ((ad[i] / bc[j]) == 24) {
					if (i >= 4)
						System.out.print("("+number[3] +printEq(i)+ number[0]);	
					else
						System.out.print("("+number[0] +printEq(i)+ number[3]);	
					if (j >= 4)
						System.out.println(") / ("+ number[2] + printEq(j) + 
						  number[1] + ") = 24");
					else
						System.out.println(") / ("+ number[1] + printEq(j) + 
						  number[2] + ") = 24");
				}
				if ((ad[i] * bc[j]) == 24) {
					if (i >= 4)
						System.out.print("("+number[3] +printEq(i)+ number[0]);
					else
						System.out.print("("+number[0] +printEq(i)+ number[3]);	
				if (j >= 4)
						System.out.println(") * ("+ number[2] + printEq(j) + 
						  number[1] + ") = 24");
					else
						System.out.println(") * ("+ number[1] + printEq(j) + 
						  number[2] + ") = 24");
				}	
			}
		}
	}
}