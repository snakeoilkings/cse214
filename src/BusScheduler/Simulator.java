package BusScheduler;
/**
 * Simulator creates a simulation of a bus route using the Bus object, and 
 * loading PassengerNodes at stops along the route and on the buses in
 * PassengerQueues
 * 
 * Homework 4
 * CSE 214
 * Recitation #6
 * TA: Kevin Flanygolts
 * Grader: Zheyuan (Jeffrey) Gao
 * 
 * @author Kathryn Blecher
 * email: kathryn.blecher@gmail.com
 * Stony Brook ID: 108871623
 */
import java.io.IOException;
import java.util.Scanner;

public class Simulator {
	
	private static int numInBuses;		//the amount of in buses
	private static int numOutBuses;		//the amount of out buses
	private static int minGroupSize;	//minimum group size
	private static int maxGroupSize;	//maximum group size
	private static double arrivalProb;	//arrival probability of passengers
	private static int duration;		//the length of the simulation
	private static int capacity;		//the capacity of the bus
	/**
	 * Constructor for the simulation. Takes user input and makes it statically
	 * available to all methods in the class.
	 * @param numInBuses
	 * @param numOutBuses
	 * @param minGroupSize
	 * @param maxGroupSize
	 * @param capacity
	 * @param arrivalProb
	 */
	public Simulator(int numInBuses, int numOutBuses, int minGroupSize, 
		 int maxGroupSize, int capacity, double arrivalProb) {
		Simulator.numInBuses = numInBuses;
		Simulator.numOutBuses = numOutBuses;
		Simulator.minGroupSize = minGroupSize;
		Simulator.maxGroupSize = maxGroupSize;
		Simulator.arrivalProb = arrivalProb;
		Simulator.capacity = capacity;
	}
	/**
	 * runs the simulation
	 * @param duration length of time of the run
	 * @return the amount of groups helped and average wait
	 */
	public double[] simulate(int duration) {
		//Initializes Simulator
		int groupsServed=0;
		int totalTimeWaited=0;
		int rest=29;
		boolean newGroup;
		int sortQueues, unloaded=0;
		PassengerQueue[] stops = new PassengerQueue[8];
		Bus[] buses = new Bus[numInBuses+numOutBuses];
		for (int i=0; i < 8; i++) 
			stops[i] = new PassengerQueue();
		//Initialize the Buses
		buses[0] = new Bus(true,"South P",0,0,capacity);
		buses[numInBuses] = new Bus(false,"South P",0,0,capacity);
		for (int i=1; i < numInBuses; i++) {
			buses[i] = new Bus(true,"South P",0,rest,capacity);
			rest+=30;
		}
		rest = 29;
		for (int i=numInBuses+1; i < numInBuses+numOutBuses; i++) {
			buses[i] = new Bus(false,"South P",0,rest,capacity);
			rest+=30;
		}
		for(int i=1; i <= duration; i++) {
			System.out.print("\n\nMinute " + i);
			for(int j=0; j < 8; j++) {
				newGroup = groupGenerated();
				//randomly generate groups
				if (newGroup) {
					PassengerNode newPass = new PassengerNode();
					newPass = createGroup(i, j);
					stops[j].enqueue(newPass);
					System.out.print("\nA group of " +newPass.getSize()+
					  " passengers arrived at " +translate(j)+ " heading to "
					  + translate(newPass.getDestination()) + ".");
				}
			}
			String next = new String();
			String busType = new String();
			int busNum, x=0, y=0, var;
			PassengerQueue temp = new PassengerQueue();
			for (int k=0; k < numInBuses + numOutBuses; k++) {
				next = translate(buses[k].getCurrentStop());
				busNum = ((buses[k].isInRoute()) ? k+1 : k+1-numInBuses);
				busType = ((buses[k].isInRoute()) ? "In" : "Out");
				var = ((buses[k].isInRoute()) ? 0 : 4);
				if (buses[k].getToNextStop()>0)
					System.out.print("\n"+busType+ " Route Bus " +busNum+ " is"
					  + " moving towards " +next+ ". Arrives in " + buses[k].
					  getToNextStop()+ " minutes.");
				if (buses[k].getToNextStop()==0 && (buses[k].getTimeToRest()
					< 2))
					System.out.print("\n" +busType+ " Route Bus " +busNum+ 
					" arrives " + "at " +translate(buses[k].getCurrentStop())+
					".");
				//unloads buses
				while(!buses[k].getPassengers(buses[k].getCurrentStop()-var).
				  isEmpty() && buses[k].getToNextStop()==0) {
					unloaded += buses[k].getPassengers(buses[k].
					  getCurrentStop()-var).peek().getSize();
					buses[k].getPassengers(buses[k].getCurrentStop()-var).
					  dequeue();	
					}
				if (unloaded > 0)
					System.out.print(" Drops off " +unloaded+ 
				      " passengers.");
				unloaded = 0;
				//loads in buses
				for(int j=var; j<var+4;j++) {
					if (!stops[j].isEmpty() && buses[k].getCurrentStop()==
					  j && buses[k].getTimeToRest() < 1 && buses[k].
					  getToNextStop()==0 && buses[k].getTimeToRest() !=-80) {
						while (!stops[j].isEmpty()) {	
							x = buses[k].getNumPassengers();
							y = stops[j].peek().getSize();
							sortQueues = stops[j].peek().getDestination()-var;
							if (x+y <= capacity ) {
								unloaded += y;
								totalTimeWaited += i-stops[j].peek().
								  getArrivalTime();
								buses[k].getPassengers((sortQueues)).enqueue
								  (stops[j].dequeue());
								groupsServed++;
							}
							else 
								temp.enqueue(stops[j].dequeue());
						}
						while (!temp.isEmpty()) 
							stops[j].enqueue(temp.dequeue());
					}
				}
				if (unloaded > 0)
					System.out.print(" Picks up "+unloaded+" passengers.");
				unloaded = 0;
				if (buses[k].getTimeToRest() > 0)
					System.out.print("\n" + busType+ " Route Bus " +busNum+ 
				      " is resting "+"at "+translate(buses[k].getCurrentStop())
				      + " for " +buses[k].getTimeToRest()+ " minutes."); 
				buses[k].tick();
			}
			//prints final output of each stop
			for(int j = 0; j < 8; j++) {
				System.out.print("\n" +j+ "(" +translate(j)+ "): ");
				while (!stops[j].isEmpty()) {
					System.out.print(stops[j].peek()+translate(stops[j].peek().
					  getDestination())+ "), " +stops[j].peek().
					  getArrivalTime()+ "]");
					temp.enqueue(stops[j].dequeue());
				}
				while (!temp.isEmpty()) 
					stops[j].enqueue(temp.dequeue());
			}
		}
		double[] waitTime = new double[2];
		waitTime[0] = groupsServed;
		System.out.print("\nGroups Served: " + waitTime[0]);
		if (groupsServed > 0) {
			waitTime[1] = (double) totalTimeWaited/groupsServed;
			System.out.printf("\nAverage Wait Time: %.2f minutes.", waitTime[1]);
		}
		else 
			System.out.print("\nAverage Wait Time: NaN");
		return waitTime;
	}
	/**
	 * makes a random number of passengers
	 * @param min minimum number of allowable passengers
	 * @param max maximum number of allowable passengers
	 * @return the randomly generated number of passengers
	 */
	private static int numGroup(int min, int max) {
		int num = (int) (Math.random() * (max-min + 1));
		num+=min;
		return num; 
	}
	/**
	 * determines whether or not a group is generated at a stop
	 * @return whether or not a group is generated
	 */
	private static boolean groupGenerated() {
		double rand = Math.random();
		if (rand < arrivalProb)
			return true;
		else
			return false;
	}
	/**
	 * if a group is generated, this method generates the passengerNode object
	 * @param arrivalTime the arrival time when the passengers were created
	 * @param start this variable determines the destination based on where 
	 *   the passenger started from (what route)
	 * @return the PassengerNode created
	 */
	private static PassengerNode createGroup(int arrivalTime, int start) {
		int destNum;
		int var1=0;
		if (start >= 4)
			var1=4;
		int numGroup = numGroup(minGroupSize, maxGroupSize);
		if (start % 4 == 0) 
			destNum = 1 + (int) (Math.random() * 3)+var1;
		else
			destNum = start+1+(int) (Math.random() * Math.abs(4+var1-start));
		if (destNum % 4 == 0)
			destNum-=4;
		PassengerNode newPassenger = new PassengerNode(numGroup, destNum, 
		  arrivalTime);
		return newPassenger;
	}
	/**
	 * translates the integer representation of a destination to it's string
	 * @param num the destination number
	 * @return the string representation of the destination number
	 */
	private static String translate(int num) {
		switch(num) {
		case 0:		return "South P";
		case 1:		return "West";
		case 2:		return "SAC";
		case 3:		return "Chapin";
		case 4:		return "South P";
		case 5:		return "PathMart";
		case 6:		return "Walmart";
		case 7:		return "Target";
		default:	return "";
		}
	}
	
	public static void  main(String[] args) throws IOException {
		Scanner input = new Scanner(System.in);
		char again='y';
		int numInBuses1, numOutBuses1, minGroupSize, maxGroupSize, capacity;
		int duration;
		double arrivalProb;
		do { 
			do {
				System.out.print("Enter the number of In Route buses: ");
				numInBuses1 = input.nextInt();
				if (numInBuses1 < 1)
					System.out.print("Error: must have atleast 1 bus.");
			} while (numInBuses1 < 1);
			do {
				System.out.print("\nEnter the number of Out Route buses: ");
				numOutBuses1 = input.nextInt();
				if (numOutBuses1 < 1)
					System.out.println("Error: must have atleast 1 bus.");
			} while (numOutBuses1 < 1);
			do {
				System.out.print("\nEnter the minimum group size of "
				  + "passengers: ");
				minGroupSize = input.nextInt();
				if (minGroupSize < 1)
					System.out.println("\nError: must have atleast 1 "
					  + "passenger.");
			} while (minGroupSize < 1);
			do {
				System.out.print("\nEnter the maximum group size of "
				  + "passengers: ");
				maxGroupSize = input.nextInt();
				if (maxGroupSize < 1)
					System.out.println("\nError: must have atleast 1 "
					  + "passenger.");
			} while (maxGroupSize < 1);
			do {
				System.out.print("\nEnter the capacity of a bus: ");
				capacity = input.nextInt();
				if (capacity < 1)
					System.out.println("\nError: Bus must be able to carry "
					  + "atleast 1 passenger.");
			} while (capacity < 1);
			do {
				System.out.print("\nEnter the arrival probability: ");
				arrivalProb = input.nextDouble();
				if (arrivalProb < 0.00000000001)
					System.out.println("\nError: Must have a probability over "
					  + "0.");
			} while (arrivalProb < 0.000000001);
			do {
				System.out.print("\nEnter the duration of the simulation: ");
				duration = input.nextInt();
				if (duration < 1)
					System.out.println("\nError: duration must be for atleast"
					  + " 1 min.");
			} while (duration < 1);
			Simulator start = new Simulator(numInBuses1, numOutBuses1,
			  minGroupSize, maxGroupSize, capacity, arrivalProb);
			start.simulate(duration);
			do {
				System.out.print("\nDo another simulation (Y/N)?");
				again = (char) System.in.read();
			} while (again!='y' && again!='Y' && again!='n' && again!='N');
		} while (again == 'y' || again == 'Y');
		System.out.print("\nProgram terminating...");
		return;
	}
}