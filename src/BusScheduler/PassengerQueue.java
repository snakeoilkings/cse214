package BusScheduler;
/**
 * PassengerQueue creates a linked list of PassengerNode objects. The list is
 * singly linked with a tail and a head. No cursor is used or needed. Instead
 * of creating a priority level/queue, the bus stores passengers in an array of
 * PassengerQueues when boarded.
 * 
 * Homework 4
 * CSE 214
 * Recitation #6
 * TA: Kevin Flanygolts
 * Grader: Zheyuan (Jeffrey) Gao
 * 
 * @author Kathryn Blecher
 * email: kathryn.blecher@gmail.com
 * Stony Brook ID: 108871623
 */

public class PassengerQueue {
	
	private int size = 0;			//size of the queue
	private PassengerNode head;		//the first passenger in list
	private PassengerNode tail;		//the last passenger in the list
	/**
	 * Empty Constructor for the PassengerQueue object 
	 */
	public PassengerQueue() {
		head = null;
		tail = null;
	}
	/**
	 * Adds a PassengerNode object to the queue
	 * @param p the new PassengerNode added
	 * postconditions: new PassengerNode is linked to PassengerQueue
	 */
	public void enqueue(PassengerNode p) {
		PassengerNode temp;
		if (head == null) {
			head = p;
			tail = head;
		}
		else {
			if (head == tail) {
				head.setNext(p);
				tail = p;
			}
			else {
				tail.setNext(p);
				tail = tail.getNext();
			}
		}
		size+=p.getSize();
	}
	/**
	 * Removes the first PassengerNode in the PassengerQueue
	 * @return the removed PassengerNode
	 * @throws EmptyQueueException if the program attempts to dequeue an empty
	 *   queue.
	 */
	public PassengerNode dequeue() throws EmptyQueueException {
		PassengerNode temp;
		if (head == null)
			throw new EmptyQueueException("List is empty. Can't dequeue.");
		if (head == tail) {
			temp = head;
			head = null;
			tail = null;
		}
		else {
			temp = head;
			head = head.getNext();
		}
		size-=temp.getSize();
		return temp;
	}
	/**
	 * determines if the list is empty
	 * @return whether queue is empty or not
	 */
	public boolean isEmpty() {
		return (head == null);
	}
	/**
	 * returns the amount of PassengerNodes in the Queue
	 * @return the size of the Queue 
	 */
	public int size() {
		return size;
	}
	/**
	 * Shows the first PassengerNode in the Queue without dequeueing it
	 * @return the PassengerNode at the start of the Queue
	 */
	public PassengerNode peek() {
		return head;
	}
}