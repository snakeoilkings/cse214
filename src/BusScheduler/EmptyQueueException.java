package BusScheduler;
/**
 * Exception class for attempting to dequeue, peek, and access other such
 * methods from an empty queue
 * @author Kathryn
 */
public class EmptyQueueException extends IllegalArgumentException{
	/**
	 * empty constructor
	 */
	public EmptyQueueException() {
		super();
	}
	/**
	 * Constuctor prints the given message when the exception is handled
	 * @param message specifically defined message
	 */
	public EmptyQueueException(String message) {
		super(message);
	}

}
