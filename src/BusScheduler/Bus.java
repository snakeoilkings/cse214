package BusScheduler;
/**
 * PassengerNode creates a linked list object that stores the passenger
 * information.
 * 
 * Homework 4
 * CSE 214
 * Recitation #6
 * TA: Kevin Flanygolts
 * Grader: Zheyuan (Jeffrey) Gao
 * 
 * @author Kathryn Blecher
 * email: kathryn.blecher@gmail.com
 * Stony Brook ID: 108871623
 */
public class Bus {

	private boolean route;			//stores whether a route is in or out 
	private static int capacity;	//max capacity of the bus
	private int numPassengers;		//the total number of passengers on the bus
	private int toNextStop;			//time to the next stop
	private int timeToRest;			//time to rest before starting a route
	private int currentStop;		//the current stop the bus is at. 
	private PassengerQueue[] passengers = new PassengerQueue[4]; //the 4 queues
									//add passengers based on their destination
	/**
	 * Empty Constructor for the Bus object
	 */
	public Bus() {
		route = true;
		toNextStop = 20;
		timeToRest = 0;
		numPassengers = 0;
		if (route)
			currentStop = 0;
		else
			currentStop = 4;
		for(int i=0; i<4;i++)
			passengers[i] = new PassengerQueue();
	}
	/**
	 * Overloaded Constructor for the Bus object
	 * @param route boolean representation of whether a route is in or out
	 * @param nextStop stores the next stop as an integer representation
	 * @param toNextStop the time to the next stop
	 * @param timeToRest the time the bus waits before starting a new route
	 * @param capacity stores the max capacity of the bus
	 */
	public Bus(boolean route, String nextStop, int toNextStop, int timeToRest,
			int capacity) {
		this.route = route;
		this.toNextStop = toNextStop;
		this.timeToRest = timeToRest;
		this.capacity = capacity;
		numPassengers = 0;
		if (route)
			currentStop = 0;
		else
			currentStop = 4;
		for(int i=0; i<4;i++)
			passengers[i] = new PassengerQueue();
	}
	/**
	 * returns the current stop
	 * @return where the bus is
	 */
	public int getCurrentStop() {
		return currentStop;
	}
	/**
	 * this method keeps track of the buses route. It decrements resting time
	 * time to next stop, and whether the time to rest needs to go back to 30
	 * or if the time to next stop goes back to 20 after an arrival
	 * postconditions: timeToRest goes back to 30 once buses reach South P, and
	 * toNextStop goes to 20 when a bus arrives at a new location. timeToRest 
	 * and timeToNext stop decrement when appropriate
	 */
	public void tick() {
		timeToRest--;
		if (timeToRest == -81) {
			timeToRest = 29;
			toNextStop = 0;
		}
		if (toNextStop == 0 && timeToRest < 0) { 
			if (currentStop < 4 && route)
				currentStop++;
			if (currentStop < 8 && !route)
				currentStop++;
			if (currentStop % 4 == 0)
				currentStop-=4;
			toNextStop = 20;
		}
		if (timeToRest < 0)
			toNextStop--;
	}
	/**
	 * sets the time the bus needs to continue to rest for
	 * @param timeToRest the resting time
	 * postconditions: timeToRest is set to input
	 */
	public void setTimeToRest(int timeToRest) {
		this.timeToRest = timeToRest;
	}
	/**
	 * returns the time the bus needs to continue to rest for
	 * @return timeToRest the resting time
	 */
	public int getTimeToRest() {
		return timeToRest;
	}
	/**
	 * gets the next stop
	 * @return the integer representation of the next stop
	 */
	public int getToNextStop() {
		return toNextStop;
	}
	/**
	 * sets the next stop
	 * @param toNextStop the next stop
	 * postconditions: toNextStop is set to the method input
	 */
	public void setToNextStop(int toNextStop) {
		this.toNextStop = toNextStop;
	}
	/**
	 * sets a queue of passengers into the bus
	 * @param pq the passengerqueue being added
	 * @param stop the stop the passengers in the queue are going
	 * postcondition: sets a given PassengerQueue to the array of
	 * passengers for the given stop
	 */
	public void setPassengers(PassengerQueue pq, int stop) {
		passengers[stop] = pq;
	}
	/**
	 * gets the queue for passengers going to the stop location
	 * @param stop where the passenger is going
	 * @return the queue of passengers for the particular stop
	 */
	public PassengerQueue getPassengers(int stop) {
		return passengers[stop];
	}
	/**
	 * returns whether a bus is an in route or out route
	 * @return true if an in route false if out route
	 */
	public boolean isInRoute() {
		return route;
	}
	/**
	 * calculates the number of passengers on the bus
	 * @return the number of passengers currently on the bus
	 * postconditions: numPassengers is set to total amount of passengers
	 */
	public int getNumPassengers() {
		int temp=0;
		for (int i=0; i < 4; i++)
			if (!passengers[i].isEmpty())
				temp += passengers[i].size();
		numPassengers = temp;
		return numPassengers;
	}
}
