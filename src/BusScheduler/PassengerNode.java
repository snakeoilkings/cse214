package BusScheduler;
/**
 * PassengerNode creates a linked list object that stores the passenger
 * information.
 * 
 * Homework 4
 * CSE 214
 * Recitation #6
 * TA: Kevin Flanygolts
 * Grader: Zheyuan (Jeffrey) Gao
 * 
 * @author Kathryn Blecher
 * email: kathryn.blecher@gmail.com
 * Stony Brook ID: 108871623
 */
public class PassengerNode {
	
	private int size;			//size of the group
	private int destination; 	//where the passengers are going
	private int arrivalTime;	//when the passengers arrived 
	private PassengerNode next;	//the next node on the list
	/**
	 * Empty Constructor creates a default PassengerNode object.
	 */
	public PassengerNode() {
		size = 1;
		destination = 0;
		arrivalTime = 0;
	}
	/**
	 * Overloaded Constructor forms a PassengerNode object based on user 
	 * defined input
	 * @param size the size of the group
	 * @param destination where the group is going
	 * @param arrivalTime When a passenger arrives, this variable is saved as 
	 * the arrival time.
	 * @throws IllegalArgumentException when size and arrivalTime are not of
	 *   possible amounts
	 */
	public PassengerNode(int size, int destination, int arrivalTime) throws
		IllegalArgumentException {
		if (size < 1)
			throw new IllegalArgumentException("Must be at least 1 passenger");
		if (arrivalTime < 0)
			throw new IllegalArgumentException("Not a valid time.");
		this.size = size;
		this.destination = destination;
		this.arrivalTime = arrivalTime;
	}
	/**
	 * sets the group size
	 * @param size amount of passengers in a group
	 */
	public void setSize(int size) throws IllegalArgumentException {
		if (size < 1)
			throw new IllegalArgumentException("Must be at least 1 passenger");
		this.size = size;
	}
	/**
	 * gets the group size
	 * @return size of group
	 */
	public int getSize() {
		return size;
	}
	/**
	 * sets the destination for a group of passengers
	 * @param destination number representation of the destination
	 */
	public void setDestination(int destination) {
		this.destination = destination;
	}
	/**
	 * gets the destination
	 * @return the number representation of the destination
	 */
	public int getDestination() {
		return destination;
	}
	/**
	 * sets the arrival time
	 * @param arrivalTime user input arrival
	 */
	public void setArrivalTime(int arrivalTime) throws IllegalArgumentException
	{
		if (arrivalTime < 0)
			throw new IllegalArgumentException("Not a valid time.");
		this.arrivalTime = arrivalTime;
	}
	/**
	 * gets the arrival time of the passenger
	 * @return arrival time
	 */
	public int getArrivalTime() {
		return arrivalTime;
	}
	/**
	 * sets the next node for the linked list to direct to
	 * @param next the next PassengerNode
	 */
	public void setNext(PassengerNode next) {
		this.next = next;
	}
	/**
	 * gets the next passenger on the list following the current one
	 * @return the next node on the list
	 */
	public PassengerNode getNext() {
		return next;
	}
	/**
	 * converts the passenger information to a string that matches the 
	 * simulator output format
	 * @return the string of the passenger info
	 */
	public String toString() {
		return "[" +size+ ", " +destination+ ", ("; 
	}
}