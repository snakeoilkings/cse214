package Stackotaire;
/**
 * CardStack extends the Stack class to hold Card objects
 * 
 * Homework 3
 * CSE 214
 * Recitation #6
 * TA: Kevin Flanygolts
 * Grader: Zheyuan (Jeffrey) Gao
 * 
 * @author Kathryn Blecher
 * email: kathryn.blecher@gmail.com
 * Stony Brook ID: 108871623
 * 
 */
import java.util.Stack;

public class CardStack extends Stack {

	private char stackType;
	private String output="";
	/**
	 * Creates an empty CardStack. Empty constructor
	 */
	public CardStack() {
		stackType = 'f';
	}
	/**
	 * Creates an empty CardStack of chosen type. Overloaded constructor
	 * @param stackType the type of stack to create
	 */
	public CardStack(char stackType) {
		super();
		this.stackType = stackType;
	}
	/**
	 * gets the stack type
	 * @return the stack type
	 */
	public char getStackType() {
		return stackType;
	}
	/**
	 * puts a new Card on the top of the stack
	 * @param newCard the new Card to be on the top of the stack
	 */
	public void push(Card newCard) {
		super.push(newCard);
		if (newCard.isFaceUp())
			output = output + "[" +newCard+ "]";
		else
			output = output + "[XX]";
	}
	/**
	 * pops the Card on the top of the stack
	 * @return the popped card
	 */
	public Card pop() {
		if (peek().getValue() == 10) {
			if (output.length() < 5)
				output = "";
			else
				output = output.substring(0,output.length()-5);
		}
		else {
			if (output.length() < 4)
				output = "";
			else
				output = output.substring(0,output.length()-4);
		}
		return (Card) super.pop();
	}
	/**
	 * Accesses the top Card on the stack without popping it.
	 * @return the Card
	 */
	public Card peek() {
		return (Card) super.peek();
	}
	/**
	 * prints out the CardStack in a certain way depending on StackType
	 */
	public void printStack() {
		String topS, topW, topF, topT;
		if (isEmpty()) {
			topS ="[  ]";
			topW ="[  ]";
			topF ="[  ]";
			topT ="[  ]";
		}
		else {
			topS = "[XX]";
			topW = "[" +peek()+ "]";
			topF = output;
			topT = "[" +peek()+ "]";
		}
		switch(stackType) {
			case 's': System.out.print(topS);
				      break;
			case 'w': System.out.print(topW);
					  break;
			case 'f': System.out.print(topF);
				      break;
			case 't': System.out.print(topT);
					  break;
		}
	}
	/**
	 * prints out the foundation stack. Overloaded constructor
	 * @param num what number foundation stack (1-4)
	 */
	public void printStack(int num) {
		String topS, topW, topF, topT;
		if (isEmpty()) 
			topF ="[F" + (num+1) + "]";
		else 
			topF = "[" +peek()+ "]";
		System.out.print(topF);
	}	
}
