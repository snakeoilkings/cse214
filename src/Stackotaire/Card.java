package Stackotaire;
/**
 * Card creates the Card object
 * 
 * Homework 3
 * CSE 214
 * Recitation #6
 * TA: Kevin Flanygolts
 * Grader: Zheyuan (Jeffrey) Gao
 * 
 * @author Kathryn Blecher
 * email: kathryn.blecher@gmail.com
 * Stony Brook ID: 108871623
 */
public class Card {
	
	private int suit;
	private int value;
	private boolean isFaceUp;
	/**
	 * Creates a new Card. Empty constructor
	 */
	public Card() {
		isFaceUp = false;
	}
	/**
	 * Creates a new card. Overloaded Constructor
	 * @param suit defines which suit 1-4
	 * @param value defines the vard value 1-13
	 */
	public Card(int suit, int value) {
		this.suit = suit;
		this.value = value;
		this.isFaceUp = false;
	}
	/**
	 * Gets a card's suit.
	 * @return the int representing the suit of the card
	 */
	public int getSuit() {
		return suit;
	}
	/**
	 * Sets a card suit
	 * @param suit (suit of card 1-4)
	 * postconditions: card suit changes to user defined suit
	 */
	public void setSuit(int suit) {
		this.suit = suit;
	}
	/**
	 * gets the value of the card from 1 - 13
	 * @return the int card value
	 */
	public int getValue() {
		return value;
	}
	/**
	 * sets the value of the card
	 * @param value new card value (1-13)
	 * postconditions: card value changed to user defined value
	 */
	public void setValue(int value) {
		this.value = value;
	}
	/**
	 * checks to see if the card is face up
	 * @return whether it is up or not
	 */
	public boolean isFaceUp() {
		return isFaceUp;
	}
	/**
	 * sets the card to face up or down
	 * @param isFaceUp determines if a card is face up
	 * postconditions: card facing is now changed to user defined facing
	 */
	public void setFaceUp(boolean isFaceUp) {
		this.isFaceUp = isFaceUp;
	}
	/**
	 * checks to see if a card is red
	 * @return if card is red or not
	 */
	public boolean isRed() {
		if (suit%2 == 1)
			return true;
		else
			return false;
	}
	/**
	 * makes a card in to a printable string
	 * @return the string representing the card
	 */
	public String toString() {
		String[] values = {" ","A","2","3","4","5","6","7","8","9","10","J",
		   "Q","K"};
		char[] suits = {' ', '\u2666','\u2663','\u2665','\u2660'};
		return values[value] + suits[suit];
	}
}
