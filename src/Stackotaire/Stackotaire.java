package Stackotaire;
/**
 * Stackotaire implements the CardStack and Card classes to make a functional
 * Solitaire-like game. The empty CardStack serves as a way to prevent users
 * from accessing stacks that don't exist without throwing an exception.
 * 
 * Homework 3
 * CSE 214
 * Recitation #6
 * TA: Kevin Flanygolts
 * Grader: Zheyuan (Jeffrey) Gao
 * 
 * @author Kathryn Blecher
 * email: kathryn.blecher@gmail.com
 * Stony Brook ID: 108871623
 */
import java.io.IOException;
import java.util.Collections;
import java.util.Scanner;

public class Stackotaire {
	
	private static CardStack pile = new CardStack('s');
	private static CardStack waste = new CardStack('w');;
	private static CardStack[] tab = new CardStack[7];
	private static CardStack[] top = new CardStack[4];
	private static CardStack empty = new CardStack('s');
	
	private static int winCheck = 0;
	/**
	 * Starts the game, creates a deck of 52 cards, 13 cards of every suit. It
	 * then sets up the game board with 4 foundation stacks, 7 tableau stacks
	 * a waste pile, and a draw pile.
	 * postconditions: pile is filled with Cards & each tab stack is loaded 
	 * with Cards.
	 */
	public static void initializeGame() {
		//creates deck of 52 cards and shuffles
		CardStack deck = new CardStack('f');
		Card nextCard = new Card();
		for(int i=1; i < 5; i++) {
			for(int j=1; j < 14; j++) {
				nextCard = new Card(i,j);
				deck.push(nextCard);  
			}
		}
		Collections.shuffle(deck);
		//creates the bottom board
		for(int i=0; i < 7; i++) 
			tab[i] = new CardStack('f');
		for(int k=0; k < 7; k++) {
			for(int i=0; i < k+1; i++) {
				if (k==i)
					deck.peek().setFaceUp(true);
				nextCard = deck.pop();
				tab[k].push(nextCard);
				}
		}
		//makes top piles
		for(int i=0; i < 4; i++) 
			top[i] = new CardStack('t');
		
		//makes draw pile with remaining cards
		pile = new CardStack('s');
		for (int i=0; i < 24; i++) {
			nextCard = deck.pop();
			pile.push(nextCard);
		}
	}
	/**
	 * prints the board to console.
	 */
	public static void display() {
		for(int i=0; i<4; i++)
			top[i].printStack(i);
		System.out.print("\tW1 ");
		waste.printStack();
		System.out.print("  ");
		pile.printStack();
		System.out.print(" " + (pile.size()) + "\n");
		for(int i=7; i >= 1; i--) {
			System.out.print("\nT" + i + " ");
			tab[i-1].printStack();
		}
		System.out.print("\n");
		if (!tab[0].isEmpty())
			autoMove(tab[0].peek(), tab[0]);
		if (!tab[1].isEmpty())
			autoMove(tab[1].peek(), tab[1]);
		if (!tab[2].isEmpty())
			autoMove(tab[2].peek(), tab[2]);
		if (!tab[3].isEmpty())
			autoMove(tab[3].peek(), tab[3]);
		if (!tab[4].isEmpty())
			autoMove(tab[4].peek(), tab[4]);
		if (!tab[5].isEmpty())
			autoMove(tab[5].peek(), tab[5]);
		if (!tab[6].isEmpty())
			autoMove(tab[6].peek(), tab[6]);
		if (!waste.isEmpty())
			autoMove(waste.peek(), waste);
	}
	/**
	 * checks to see if the player has won. winCheck increments every time a
	 * card is removed from the waste pile or a card is turned face up on the
	 * board. The game starts with 7 face up cards, so once winCheck hits 45,
	 * the game is over and the player has won.
	 * @return whether the player has won or not
	 */
	public static boolean checkForWin() {
		if (winCheck == 45) {
			System.out.print("\n\nYou won!! Good job! Would you like to play "
			  + "again? (Y/N): ");
			winCheck = 0;
			return true;
		}
		else
			return false;
	}
	/**
	 * draws a card from the top of the draw pile and puts it on the top of the
	 * waste pile
	 * postconditions: a Card is popped from pile and pushed in to waste.
	 */
	public static void draw() {
		Card k = new Card();
		int deckSize = waste.size();
		if (pile.isEmpty()) {
			for(int i=0; i < deckSize;i++) {
				k = waste.pop();
				pile.push(k);
			}
		}
		if (!pile.isEmpty()) {
			k = pile.pop();
			waste.push(k);
		}
		display();
	}
	/**
	 * moves a card from one stack to another
	 * @param target1 the source stack
	 * @param target2 the target stack
	 * postconditions: the top car is popped from the source stack and pushed
	 * on to the target stack
	 * @return
	 */
	public static boolean move (CardStack target1, CardStack target2) {
		Card card;
		if (!target2.isEmpty() && (target1.getStackType() == 't')) {
			winCheck--;
		}
		//moves a card to foundation pile
		if (target2.getStackType() == 't') {
			if ((target2.isEmpty()) && (target1.peek().getValue() == 1)) {
				card = target1.pop();
				target2.push(card);
				if (!target1.isEmpty() || target1.getStackType()=='w') {
					if (!target1.isEmpty()) {
						card = target1.pop();
						card.setFaceUp(true);
						target1.push(card);
					}
					winCheck++;
				}
				card = target2.pop();
				card.setFaceUp(true);
				target2.push(card);
				return true;
			}
			if ((target2.isEmpty()) && (target1.peek().getValue() != 1))
				return false;
			if ((target2.peek().getValue()+1 == target1.peek().getValue())
			   && (target2.peek().getSuit() == target1.peek().getSuit())) {
				card = target1.pop();
				target2.push(card);
				if (!target1.isEmpty() || target1.getStackType()=='w') {
					if (!target1.isEmpty()) {
						card = target1.pop();
						card.setFaceUp(true);
						target1.push(card);
					}
					winCheck++;
				}
				card = target2.pop();
				card.setFaceUp(true);
				target2.push(card);
				return true;
			}
			else
				return false;
		}
		// moves cards between tableaus or waste
		if (target2.getStackType() == 'f') {
			if ((target2.isEmpty()) && (target1.peek().getValue() == 13)) {
				card = target1.pop();
				card.setFaceUp(true);
				target2.push(card);
				if ((!target1.isEmpty() && (target1.getStackType() != 't')) ||
				  target1.getStackType()=='w') {
					if (!target1.isEmpty()) {
						card = target1.pop();
						card.setFaceUp(true);
						target1.push(card);
					}
					winCheck++;
				}
				if (!target1.isEmpty() && (target1.getStackType() == 't')) {
					winCheck--;
				}
				return true;
			}
			if ((target2.isEmpty()) && (target1.peek().getValue() != 13))
				return false;
			if ((target2.peek().getValue() == target1.peek().getValue()+1)
			   && (target2.peek().isRed() != target1.peek().isRed())) {
				card = target1.pop();
				card.setFaceUp(true);
				target2.push(card);
				if ((!target1.isEmpty() && (target1.getStackType() != 't')) ||
				  target1.getStackType()=='w') {
					if (!target1.isEmpty()) {
						card = target1.pop();
						card.setFaceUp(true);
						target1.push(card);
					}
					winCheck++;
				}
				return true;
			}
			else
				return false;
		}
		else
			return false;
	}
	/**
	 * moves a stack of more than one card over
	 * @param target1 the first pile (source)
	 * @param target2 the second pile (target)
	 * @param n how many cards to move
	 * postconditions: the cards are popped off the source stand and pushed
	 * on to the target stack
	 * @return whether or not the move was able to occur
	 */
	public static boolean moveN(CardStack target1, CardStack target2, int n) {
		Card check = null, card;
		CardStack temp = new CardStack();
		for(int i=0; i <n; i++) {
			if (target1.isEmpty()) {
				for(int j=0; j < i-1; j++) {
					check = temp.pop();
					target1.push(check);
				}
				return false;
			}
			check = target1.pop();
			temp.push(check);
		}
		if ((target2.isEmpty()) && (check.getValue() == 13)) {
			for(int i=0; i < n; i++) 
				target2.push(temp.pop());
			if (!target1.isEmpty()) {
				card = target1.pop();
				card.setFaceUp(true);
				target1.push(card);
				winCheck++;
			}
			return true;
		}
		if ((target2.isEmpty()) && (check.getValue() != 13)) {
			
			for(int i=0; i <n; i++) {
				check = temp.pop();
				target1.push(check);
			}
			return false;
		}
		if ((target2.peek().getValue() == check.getValue()+1)
		  && (target2.peek().isRed() != check.isRed())) {
			for(int i=0; i < n; i++) 
				target2.push(temp.pop());
			if (!target1.isEmpty()) {
				card = target1.pop();
				card.setFaceUp(true);
				target1.push(card);
				winCheck++;
			}
			return true;
		}
		else
			for(int i=0; i <n; i++) {
				check = temp.pop();
				target1.push(check);
			}
			return false;
	}
	/**
	 * cuts up the string of the user instruction and transforms it into a 
	 * readable instruction for the program, and then deploys the instruction.
	 * @param instruction user defined instruction
	 */
	public static void readInstruction(String instruction) {
		CardStack target1, target2;
		boolean moveN = false;
		String[] moves;
		instruction = instruction.toLowerCase();
		
		if (instruction.equals("draw")) {
			draw();
			return;
		}
		
		if (instruction.equals("quit")) {
			return;
		}
		
		if (instruction.equals("restart")) {
			return;
		}
		
		if(instruction.charAt(4) == 'n')  {
			moves = new String[4];
			moveN = true;
		}
		else {
			moves = new String[3];
		}
		moves = instruction.split(" ");
		
		char c = moves[1].charAt(0);
		int num = moves[1].charAt(1) - '1';

		target1 = whichStack(c, num);
		
		c = moves[2].charAt(0);
		num = moves[2].charAt(1) - '1';
		
		target2 = whichStack(c, num);
		
		boolean movePossible;
		
		if (moveN) {
			int howMany = Integer.valueOf(moves[3]);
			movePossible = moveN(target1, target2, howMany);
		}
		else
			movePossible = move(target1, target2);
	}
	/**
	 * translates which stack the user wants to target based on their entry
	 * into a readable choice for the program.
	 * @param c the first character, which defines the stack type
	 * @param num the number of the stack index. if the stack isn't an array
	 * such as the case of waste and pile, it is ignored.
	 * @return the target CardStack
	 */
	public static CardStack whichStack(char c, int num) {
		switch(c) {
			case 'w':		return waste;
			case 'p':		return pile;
			case 't':		return tab[num];
			case 'f':		return top[num];
			default:		return empty;
		}
	}
	/**
	 * translates a stack back to it's name for printing to console
	 * @param stack the stack being translated
	 * @return a string of the moved card
	 */
	public static String stringStack(CardStack stack) {
		if (stack == waste)
			return "W1";
		if (stack == tab[0])
			return "T1";
		if (stack == tab[1])
			return "T2";
		if (stack == tab[2])
			return "T3";
		if (stack == tab[3])
			return "T4";
		if (stack == tab[4])
			return "T5";
		if (stack == tab[5])
			return "T6";
		else 
			return "T7";
	}
	/**
	 * auto moves available cards to the foundation pile
	 * @param card a face up card
	 * @param target1 the pile the face up card is from
	 * postconditions: if it returns true the card was pushed from target1 to
	 * the foundation pile
	 * @return whether or not a card was moved
	 */
	public static boolean autoMove(Card card, CardStack target1) {
		boolean wasMoved;
		String source = stringStack(target1);
		wasMoved = move(target1, top[0]);
		if(wasMoved) {
			display();
			System.out.println("["+card+ "] was moved from "+source+ " to F1");
			return true;
		}
		wasMoved = move(target1, top[1]);
		if(wasMoved) {
			display();
			System.out.println("["+card+ "] was moved from " +source+" to F2");
			return true;
		}
		wasMoved = move(target1, top[2]);
		if(wasMoved) {
			display();
			System.out.println("[" +card+ "] was moved from "+source+" to F1");
			return true;
		}
		wasMoved = move(target1, top[3]);
		if(wasMoved) {
			display();
			System.out.println("["+card+ "] was moved from "+source+ " to F1");
			return true;
		}
		else
			return false;
	}
	
	public static void main(String[] args) throws IOException {
		char again = 'y';
		
		while(again == 'y' || again == 'Y') {
			initializeGame();
			display();
			
			Scanner input = new Scanner(System.in);
			String instruction = new String();
			boolean win = checkForWin();
		
			while(!win){
			  instruction = input.nextLine();
			  if (instruction.equals("draw") ||(instruction.matches("move[\\s]"
				+ "[a-z][\\d][\\s][a-z][\\d]")) || (instruction.matches("moven"
				+ "[\\s][a-z][\\d][\\s][a-z][\\d][\\s][\\d][\\d]")) || 
				(instruction.matches("moven[\\s][a-z][\\d][\\s][a-z][\\d][\\s]"
			    + "[\\d]")) || (instruction.equals("quit")) || 
			    (instruction.equals("restart")))
				readInstruction(instruction);
				if (instruction.equals("quit")) {
					System.out.println("\nYou lose.\n");
					win = true;
					again = 'n';
				}
				else if (instruction.equals("restart")) {
					System.out.println("\nYou lose.\n\nRestarting...\n");
					win = true;
					again = 't';
				}
				else
					display();
			}
		if (again == 'y')
			again = (char) System.in.read();
		if (again == 't')
			again = 'y';
		}
	System.out.print("\nThanks for playing. Program terminated.");
	return;
	}
}
