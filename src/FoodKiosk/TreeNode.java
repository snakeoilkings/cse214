package FoodKiosk;
/**
 * TreeNode creates a TreeNode object that contains a name, data (selection),
 * and a message to show the user. Max branches a node can have are 3 
 * 
 * Homework 5
 * CSE 214
 * Recitation #6
 * TA: Kevin Flanygolts
 * Grader: Zheyuan (Jeffrey) Gao
 * 
 * @author Kathryn Blecher
 * email: kathryn.blecher@gmail.com
 * Stony Brook ID: 108871623
 */

public class TreeNode {

	private String name = "";       //node identifier
	private String selection = "";  //node content
	private String message = "";    //node message to user
	private TreeNode left;          //left node
	private TreeNode middle;	    //middle node
	private TreeNode right;         //right node
	/**
	 * Empty constructor for the TreeNode
	 */
	public TreeNode() {
		left = null;
		middle = null;
		right = null;
	}
	/**
	 * Overloaded constructor for the TreeNode to specification
	 * @param name node name
	 * @param selection node info
	 * @param message node message
	 */
	public TreeNode(String name, String selection, String message) {
		this.name = name;
		this.selection = selection;
		this.message = message;
		left = null;
		middle = null;
		right = null;
	}
	/**
	 * gets the name of the node
	 * @return name of the node
	 */
	public String getName() {
		return name;
	}
	/**
	 * sets name of the node
	 * @param name of node
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * gets the message of the node
	 * @return message to user
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * sets the message of the node
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * gets the selection of the node
	 * @return selection
	 */
	public String getSelection() {
		return selection;
	}
	/**
	 * sets the selection of the node
	 * @param selection
	 */
	public void setSelection(String selection) {
		this.selection = selection;
	}
	/**
	 * returns the left node of a tree
	 * @return left TreeNode
	 */
	public TreeNode getLeft() {
		return left;
	}
	/**
	 * sets the left node
	 */
	public void setLeft(TreeNode left) {
		this.left = left;
	}
	/**
	 * returns the middle node of a tree
	 */
	public TreeNode getMiddle() {
		return middle;
	}
	/**
	 * sets the middle node of a tree
	 */
	public void setMiddle(TreeNode middle) {
		this.middle = middle;
	}
	/**
	 * returns the right node
	 */
	public TreeNode getRight() {
		return right;
	}
	/**
	 * sets the right node
	 */
	public void setRight(TreeNode right) {
		this.right = right;
	}
	/**
	 * returns whether or not a node is a leaf
	 * @return
	 */
	public boolean isLeaf() {
		return (left == null && right == null && middle == null);
	}
	/**
	 * prints the content of a node when attempted to print
	 */
	public String toString() {
		return selection +"\n";
	}
}
