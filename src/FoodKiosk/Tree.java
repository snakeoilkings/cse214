package FoodKiosk;
/**
 * Tree creates a tree object loaded with three nodes with a max of 3 branches
 * for each node
 * 
 * Homework 5
 * CSE 214
 * Recitation #6
 * TA: Kevin Flanygolts
 * Grader: Zheyuan (Jeffrey) Gao
 * 
 * @author Kathryn Blecher
 * email: kathryn.blecher@gmail.com
 * Stony Brook ID: 108871623
 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.Stack;

public class Tree {

	private TreeNode root;  //stores the top node
	/**
	 * empty constructor for tree object
	 */
	public Tree() {
		root = null;
	}
	/**
	 * overloaded constructor for tree object
	 * @param root
	 */
	public Tree(TreeNode root) {
		this.root = root;
	}
	/**
	 * returns the root of a tree
	 * @return root
	 */
	public TreeNode getRoot() {
		return root;
	}
	/**
	 * sets the root only if the root is null
	 * @param root
	 */
	public void setRoot(TreeNode root) {
		if (root==null)
			this.root = root;
	}
	/**
	 * traverses a tree to find a node, implemented with a lot of stacks
	 * @param name the name of the node being searched
	 * @return the node or null if not found
	 */
	public TreeNode findNode(String name) {
		TreeNode node = root;
		Stack<TreeNode> nameStack = new Stack();
		Stack skipStack = new Stack();
		int skip = 0;
		nameStack.push(root);
		skipStack.push(-99);
		if (root.getMiddle()!=null) {
			nameStack.push(root);
			skipStack.push(1);
		}
		if (root.getRight()!=null) {
			nameStack.push(root);
			skipStack.push(2);
		}
		while (skip >= 0) {
			if (node.getName().equals(name))
				return node;
			else {
				if (skip < 1 && node.getLeft() != null) {
					skip=0;
					nameStack.push(node);
					skipStack.push(1);
					node = node.getLeft();
				}
				else if (skip < 2 && node.getMiddle()!= null) {
					skip = 0;
					nameStack.push(node);
					skipStack.push(2);
					node = node.getMiddle();
				}
				else if (skip < 3 && node.getRight() != null) {
					skip = 0;
					nameStack.push(node);
					skipStack.push(3);
					node = node.getRight();
				}
				else {
				node = nameStack.pop();
				node = nameStack.pop();
				skip = (int) skipStack.pop();
				skip = (int) skipStack.pop();
				}
			}
		}
		return null;
	}
	/**
	 * adds a node to the tree
	 * @param name name of the node
	 * @param selection the contents of the node
	 * @param message the message to the user
	 * @param parentNode the node you are adding to
	 * @return true if the node is added, false otherwise
	 */
	public boolean addNode(String name, String selection, String message, 
	  String parentNode) {
		TreeNode newNode = new TreeNode(name, selection, message);
		TreeNode node = findNode(parentNode);
		if (node == null)
			return false;
		if(node.getLeft()==null) {
			node.setRight(newNode);
			node.setLeft(node.getMiddle());
			node.setMiddle(null);
			return true;
		}
		if(node.getMiddle()==null) {
			node.setMiddle(node.getRight());
			node.setRight(newNode);
			return true; 
		}
		return false;
	}
	/**
	 * prints the 3 choices of a node to a user
	 * @param parentInfo name of the node having branches printed
	 */
	public void printMenu(String parentInfo) {
		TreeNode node = findNode(parentInfo);
		int i = 1;
		if(node.getLeft()!=null) {
			System.out.println(i + ". "+ node.getLeft().getSelection());
			i++;
		}
		if(node.getMiddle()!=null) {
			System.out.println(i +". "+ node.getMiddle().getSelection());
			i++;
		}
		if(node.getRight()!=null) 
			System.out.println(i +". "+ node.getRight().getSelection());
		System.out.println("0. Exit Session");
	}
	/**
	 * starts an ordering simulation from the tree
	 */
	public void beginSession() {
		TreeNode node = root;
		int choice = -1, i = 0;
		Scanner input = new Scanner(System.in);
		String orderMessage = "The order at ";
		while ((node.getLeft()!=null || node.getMiddle()!=null || 
		  node.getRight()!=null)) {
			System.out.println(node.getMessage());
			printMenu(node.getName());
			System.out.print("Choice: ");
			choice = input.nextInt();
			System.out.println();
			switch (choice) {
		    	case 1:     if(node.getLeft()!=null) 
		    					node = node.getLeft(); 
		    				else
		    					node = node.getMiddle();
		    				break;
		    	case 2:     if(node.getMiddle()!=null) 
		    					node = node.getMiddle();
		    				else
		    					node = node.getRight();
		    				break;
		    	case 3:     if(node.getRight()!=null) 
		    					node = node.getRight();
		    				break;
		    	case 0:     return;
		    	default:    System.out.println("Invalid selection."); 
			}
			if (i==0)
				orderMessage+=node.getSelection() + ": ";
			if (i != 0)
				orderMessage+= node.getSelection() + ", ";
			i++;
		}
		orderMessage= orderMessage.substring(0,orderMessage.length()-2);
		System.out.print(orderMessage);
		System.out.print(" has been sent to the Kitchen. Total amount will be "
				+ node.getMessage() + "\n");
	}
}
