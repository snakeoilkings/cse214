package FoodKiosk;
/**
 * Kiosk creates a food ordering simulator. It can print a menu, load a menu
 * and process an order.
 * 
 * Homework 5
 * CSE 214
 * Recitation #6
 * TA: Kevin Flanygolts
 * Grader: Zheyuan (Jeffrey) Gao
 * 
 * @author Kathryn Blecher
 * email: kathryn.blecher@gmail.com
 * Stony Brook ID: 108871623
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.Stack;

public class Kiosk {
	
	private static int size=0;		//stores the amount of nodes in the tree
	private static int depth=0;		//store the max depth of the tree

	/**
	 * loads a file specified by the user and creates a tree with the info
	 * @return the tree
	 * @throws IOException if input is invalid
	 */
	public static Tree load() throws IOException {
		Scanner input = new Scanner(System.in);
		System.out.print("\nChoose file for tree data: ");
		String fileName = new String();
		fileName = input.nextLine();
		fileName = System.getProperty("java.class.path") + "\\" +fileName;
		File someFile = new File(fileName);
		if (!someFile.exists()){
			System.out.print("\nfile doesn't exist. returning to main menu..."
			  + "\n");
			return null;
		}
		FileInputStream fis = new FileInputStream(fileName); 
		InputStreamReader inStream = new InputStreamReader(fis);
		BufferedReader reader = new BufferedReader(inStream);
		String name, selection, message, pos_in, parentName;
		int numNodes, whichNode;
		//make root
		name = reader.readLine();
		selection = reader.readLine();
		message = reader.readLine();
		pos_in = reader.readLine();
		TreeNode root = new TreeNode(name, selection, message);
		Tree tree = new Tree(root);
		size++;
		numNodes = Character.getNumericValue(pos_in.charAt(pos_in.length()-1));
		//make rest of nodes
		while (pos_in!=null) {
			for (int i=0; i < numNodes; i++) {
				name = reader.readLine();
				selection = reader.readLine();
				message = reader.readLine();
				TreeNode newNode = new TreeNode(name, selection, message);
				if (numNodes==3) {
					if (i==0) 
						root.setLeft(newNode);
					if (i==1)
						root.setMiddle(newNode);
					if (i==2)
						root.setRight(newNode);
				}
				if (numNodes==2) {
					if (i==0)
						root.setLeft(newNode);
					if (i==1)
						root.setRight(newNode);
				}
				if (numNodes==1) 
					root.setMiddle(newNode);
				size++;
			}
			pos_in = reader.readLine();
			if (pos_in == null)
				break;
			numNodes = Character.getNumericValue(pos_in.charAt(pos_in.length()
			  -1));
			parentName = pos_in.substring(0, pos_in.length()-2);
			root = tree.findNode(parentName);		
		}
		System.out.print("\nTree loaded...\n");
		return tree;
	}
	/**
	 * prints the menu
	 * @param tree
	 */
	public static void print(Tree tree) {
		String[] menuList = new String[size];
		TreeNode node = tree.getRoot();
		Stack<TreeNode> nameStack = new Stack();
		Stack skipStack = new Stack();
		Stack stringStack = new Stack();
		Stack temp = new Stack();
		int skip = 0;
		int i = 0;
		String check;
		int maxDepth = 0;
		nameStack.push(node);
		skipStack.push(-99);
		while (skip >= 0) {
			if (skip < 1 && node.getLeft() != null) {
				skip=0;
				nameStack.push(node);
				skipStack.push(1);
				stringStack.push(node.getSelection()+ "%");
				node = node.getLeft();
				depth++;
			}
			else if (skip < 2 && node.getMiddle()!= null) {	
				if (skip < 1)
					depth++;
				skip = 0;
				nameStack.push(node);
				skipStack.push(2);
				stringStack.push(node.getSelection() + "%");
				node = node.getMiddle();
			}
			else if (skip < 3 && node.getRight() != null) {
				if (skip < 1)
					depth++;
				skip = 0;
				nameStack.push(node);
				skipStack.push(3);
				stringStack.push(node.getSelection() + "%");
				node = node.getRight();
			}
			else {
				if (node.isLeaf()) {
					while (!stringStack.isEmpty()) {
						check = (String) stringStack.pop();
						temp.push(check);
					}
					while (!temp.isEmpty()) {
						stringStack.push(temp.pop());
						menuList[i] += stringStack.peek();
					}
					stringStack.pop();
					menuList[i] += node.getSelection() + "%";
					menuList[i] += node.getMessage();
				}
				node = nameStack.pop();
				skip = (int) skipStack.pop();
				if (depth > maxDepth)
					maxDepth = depth;
				if (!stringStack.isEmpty() && skip==3) { 
					stringStack.pop();
					depth--;
				}
			}		
		depth = maxDepth;
		if (menuList[i]!=null)
			i++;
		}
		String output = new String("");
		String foodSelection = new String("");
		int j=0;
		System.out.println("Menu:");
		output = String.format("%-20s%-60s%-25s","Dining","Selection","Price");
		System.out.println(output);
		System.out.println("--------------------------------------------------"
				+ "-----------------------------------");
		while (j < size && menuList[j] != null) {
		String[] omfg = menuList[j].split("\\%");
		String restaurant = omfg[1];
		String price = omfg[omfg.length-1];
		String item = new String("");
		for (int k=2; k < omfg.length-1; k++)
			item+= omfg[k] + ", ";
		item = item.substring(0,item.length()-2);
		output = String.format("%-20s%-60s%-25s", restaurant, item, price);
		System.out.println(output);
		j++;
		}
	}
	/**
	 * main program
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		char choice = '0';
		String pick;
		Tree tree = new Tree();
		Scanner input = new Scanner(System.in);
		while (choice != 'q' || choice != 'Q') {
			System.out.print("\nL) Load a Tree\nP) Print menu\nS) Start servic"
			  + "e\nQ) Quit\nChoice: ");
			pick = input.next();
			choice = pick.charAt(0);
			switch (choice) {
				case 'L' :
				case 'l' :	tree = load();
							break;
				case 'P' :  
				case 'p' :  if (tree.findNode("root") != null)
						    print(tree);
					    	break;
				case 'S' :
				case 's' :  if (tree.findNode("root") != null)
							tree.beginSession();
							break;
				case 'Q' :
				case 'q' :  System.out.print("\nTerminating Program");
							return;
			}
		}
	}
}
